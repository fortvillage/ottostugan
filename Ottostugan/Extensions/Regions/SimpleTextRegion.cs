﻿using System;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using Ottostugan.Extensions.PageTypes;
using Piranha.Extend;

namespace Ottostugan.Extensions.Regions
{
    [Export(typeof(IExtension))]
    [ExportMetadata("InternalId", "SimpleTextRegion")]
    [ExportMetadata("Name", "SimpleTextRegion")]
    [ExportMetadata("ResourceType", typeof(Resources.Extensions))]
    [ExportMetadata("Type", ExtensionType.Region)]
    [Serializable]
    public class SimpleTextRegion : Extension
    {
        [Display(Name = "SimpleTextRegion", ResourceType = typeof(Resources.Extensions))]
        public string Text { get; set; }
    }
}