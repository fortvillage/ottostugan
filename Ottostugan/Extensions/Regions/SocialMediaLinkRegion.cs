﻿using System;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Ottostugan.DAL;
using Piranha.Extend;

namespace Ottostugan.Extensions.Regions
{
    [Export(typeof(IExtension))]
    [ExportMetadata("InternalId", "SocialMediaLinkRegion")]
    [ExportMetadata("Name", "SocialMediaLinkRegion")]
    [ExportMetadata("ResourceType", typeof(Resources.Extensions))]
    [ExportMetadata("Type", ExtensionType.Region)]
    [Serializable]
    public class SocialMediaLinkRegion : Extension
    {
        [Display(ResourceType = typeof (Resources.Extensions), Name = "SocialMediaLinkRegion_Text_Social_Media_Link")]
        public int? Id { get; set; }

        public SelectList SocialMediaLinks { get; private set; }

        public override void InitManager(object model)
        {
            using (var db = new OttostuganContext())
            {
                var pages = db.SocialMediaLinks.ToList();

                SocialMediaLinks = new SelectList(pages, "Id", "Name");
            }

            base.InitManager(model);
        }
    }
}