﻿using System;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Ottostugan.DAL;
using Piranha.Extend;

namespace Ottostugan.Extensions.Regions
{
    [Export(typeof(IExtension))]
    [ExportMetadata("InternalId", "PriceListRegion")]
    [ExportMetadata("Name", "PriceListRegion_Id_Price_List")]
    [ExportMetadata("ResourceType", typeof(Resources.Extensions))]
    [ExportMetadata("Type", ExtensionType.Region)]
    [Serializable]
    public class PriceListRegion : Extension
    {
        [Display(ResourceType = typeof (Resources.Extensions), Name = "PriceListRegion_Id_Price_List")]
        public int? Id { get; set; }

        public SelectList PriceLists { get; private set; }

        public override void InitManager(object model)
        {
            using (var db = new OttostuganContext())
            {
                var priceLists = db.PriceLists
                    .Include("PriceListRows")
                    .ToList();

                PriceLists = new SelectList(priceLists, "Id", "Name");
            }

            base.InitManager(model);
        }
    }
}