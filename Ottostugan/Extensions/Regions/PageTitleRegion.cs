﻿using System;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using Piranha.Extend;

namespace Ottostugan.Extensions.Regions
{
    [Export(typeof(IExtension))]
    [ExportMetadata("InternalId", "PageTitleRegion")]
    [ExportMetadata("Name", "PageTitleRegion")]
    [ExportMetadata("ResourceType", typeof(Resources.Extensions))]
    [ExportMetadata("Type", ExtensionType.Region)]
    [Serializable]
    public class PageTitleRegion : Extension
    {
        [Display(Name = "PageTitleRegion", ResourceType = typeof(Resources.Extensions))]
        public string Title { get; set; }
    }
}