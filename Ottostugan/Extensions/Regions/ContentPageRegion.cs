﻿using System;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Ottostugan.Extensions.PageTypes;
using Piranha;
using Piranha.Extend;

namespace Ottostugan.Extensions.Regions
{
    [Export(typeof(IExtension))]
    [ExportMetadata("InternalId", "ContentPageRegion")]
    [ExportMetadata("Name", "ContentPageRegionName")]
    [ExportMetadata("ResourceType", typeof(Resources.Extensions))]
    [ExportMetadata("Type", ExtensionType.Region)]
    [Serializable]
    public class ContentPageRegion : Extension
    {
        [Display(ResourceType = typeof(Resources.Extensions), Name = "ContentPageRegionName")]
        public Guid? Id { get; set; }

        public SelectList ContentPages { get; private set; }

        public override void InitManager(object model)
        {
            using (var db = new DataContext())
            {
                var pages = db.Pages
                    .Include("Template")
                    .Where(p=>p.Template.Type == (typeof(ContentPage)).FullName)
                    .ToList();

                ContentPages = new SelectList(pages, "Id", "Title");
            }

            base.InitManager(model);
        }
    }
}