using Ottostugan.Models;
using Piranha.Models;

namespace Ottostugan.Extensions.PageModels
{
    public class GuestBookPageModel : PageModel
    {
        public GuestFeedback[] Feedbacks { get; set; }
    }
}