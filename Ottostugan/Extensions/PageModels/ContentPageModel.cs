using System.Linq;
using Ottostugan.DAL;
using Ottostugan.Extensions.Regions;
using Ottostugan.Models;
using Piranha.Extend.Regions;
using Piranha.Models;
using Piranha.WebPages;

namespace Ottostugan.Extensions.PageModels
{
    public class ContentPageModel : PageModel
    {
        public string Title { get; set; }
        public string Text { get; set; }
        
        public string StartPageTitle { get; set; }
        public string StartPageText { get; set; }
        public ImageModel StartPageImage { get; set; }

        public SocialMediaLink Link1 { get; set; }
        public SocialMediaLink Link2 { get; set; }
        public SocialMediaLink Link3 { get; set; }

        public void InitProperties()
        {
            Title = Region<PageTitleRegion>("Title").Title;
            Text = Region<HtmlRegion>("Content").ToHtmlString();
            
            var startPageImageRegion = Region<ImageRegion>("StartPageImage");
            StartPageImage = new ImageModel()
            {
                Description = startPageImageRegion.Description,
                Id = startPageImageRegion.Id
            };

            var startPageContentRegion = Region<TextRegion>("StartPageContent");
            StartPageTitle = startPageContentRegion.Title;
            StartPageText = startPageContentRegion.Body;

            using (var db = new OttostuganContext())
            {
                var region1 = Region<SocialMediaLinkRegion>("SocialMediaLink1");

                if (region1.Id.HasValue)
                {
                    Link1 = db.SocialMediaLinks.First(l => l.Id == region1.Id);
                }

                var region2 = Region<SocialMediaLinkRegion>("SocialMediaLink2");

                if (region2.Id.HasValue)
                {
                    Link2 = db.SocialMediaLinks.First(l => l.Id == region2.Id);
                }

                var region3 = Region<SocialMediaLinkRegion>("SocialMediaLink3");

                if (region3.Id.HasValue)
                {
                    Link3 = db.SocialMediaLinks.First(l => l.Id == region3.Id);
                }

            }

        }
    }
}