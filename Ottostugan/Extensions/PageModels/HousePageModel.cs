﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Ottostugan.Extensions.PageTypes;
using Piranha.Models;

namespace Ottostugan.Extensions.PageModels
{
    public class HousePageModel : PageModel
    {
        public IList<PageModel> Apartments { get; set; }

        public void LoadApartments()
        {
            using (var db = new Piranha.DataContext())
            {
                var apartments = db.Pages
                    .Include("Template")
                    .Where(p => p.ParentId == Page.Id && p.Template.Type == (typeof (ApartmentPage)).FullName)
                    .OrderBy(p=>p.Seqno)
                    .ToList();

                if (apartments.Count > 0)
                {
                    var query = from childPage in apartments
                        select GetById(childPage.Id);

                    Apartments = query.ToList();
                }
            }
        }
    }
}