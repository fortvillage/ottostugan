﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Ottostugan.Extensions.PageTypes;
using Piranha.Models;

namespace Ottostugan.Extensions.PageModels
{
    public class HouseOverviewPageModel : PageModel
    {
        public IList<PageModel> Houses { get; set; }

        public PageModel SearchPage { get; set; }

        public void LoadSearchPage()
        {
            using (var db = new Piranha.DataContext())
            {
                var searchPage = db.Pages
                    .Include("Template")
                    .Where(p=> p.Template.Type == (typeof(SearchPage)).FullName)
                    .OrderBy(p => p.Seqno)
                    .FirstOrDefault();

                if (searchPage != null)
                {
                    SearchPage = GetById(searchPage.Id);
                }
            }
        }

        public void LoadHouses()
        {
            using (var db = new Piranha.DataContext())
            {
                var houses = db.Pages
                    .Include("Template")
                    .Where(p => p.ParentId == Page.Id && p.Template.Type == (typeof(HousePage)).FullName)
                    .OrderBy(p => p.Seqno)
                    .ToList();

                if (houses.Count > 0)
                {
                    var query = from childPage in houses
                                select GetById(childPage.Id);

                    Houses = query.ToList();
                }
            }
        }
    }
}