﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Ottostugan.Extensions.PageTypes;
using Ottostugan.Models;
using Ottostugan.Services;
using Ottostugan.ViewModels;
using Piranha.Models;

namespace Ottostugan.Extensions.PageModels
{
    public class SearchPageModel : PageModel
    {
        private readonly IApartmentsCachedService _apartmentsCachedService;

        public IList<PageModel> Houses { get; set; }

        public Dictionary<Guid, IList<Tuple<decimal, PageModel>>> Apartments { get; set; }

        public IList<BookingCalendarEvent> Bookings { get; set; }

        public SelectList AdultsCountSelectList { get; private set; }

        public SearchType SearchType { get; set; }

        public SearchPageModel()
        {
            _apartmentsCachedService = new ApartmentsCachedService();
            Initialize();
        }

        private void Initialize()
        {
            using (var db = new Piranha.DataContext())
            {
                var allApartments = _apartmentsCachedService.GetApartmetPages();

                if (allApartments.Length == 0)
                {
                    AdultsCountSelectList = new SelectList(Enumerable.Range(0, 0).ToArray());
                    return;
                }

                var maxAdultBedsCount = allApartments
                    .Where(p => 
                        p.Properties.Any(prop => prop.Name == "Adult beds" && !string.IsNullOrEmpty(prop.Value))
                        || p.Properties.Any(prop => prop.Name == "Extra beds" && !string.IsNullOrEmpty(prop.Value))
                    )
                    .Max(p =>
                    {
                        var hasAdultBeds =
                            p.Properties.Any(prop => prop.Name == "Adult beds" && !string.IsNullOrEmpty(prop.Value));

                        var adultBedsCount = hasAdultBeds
                            ? int.Parse(p.Properties.First(prop => prop.Name == "Adult beds").Value)
                            : 0;

                        var hasExtraBeds =
                            p.Properties.Any(prop => prop.Name == "Extra beds" && !string.IsNullOrEmpty(prop.Value));

                        var extraBedsCount = hasExtraBeds
                            ? int.Parse(p.Properties.First(prop => prop.Name == "Extra beds").Value)
                            : 0;

                        return adultBedsCount + extraBedsCount;
                    });


                var selectedValue = maxAdultBedsCount > 1 ? 2 : maxAdultBedsCount;
                AdultsCountSelectList = new SelectList(Enumerable.Range(1, maxAdultBedsCount).ToArray(), selectedValue: selectedValue);

            }

            SearchType = SearchType.SplittedWeek;
        }

        public void LoadHouses()
        {
            using (var db = new Piranha.DataContext())
            {
                var houses = db.Pages
                    .Include("Template")
                    .Where(p => p.Template.Type == (typeof(HousePage)).FullName)
                    .OrderBy(p => p.Seqno)
                    .ToList();

                if (houses.Count > 0)
                {
                    var query = from childPage in houses
                        select GetById(childPage.Id);

                    Houses = query.ToList();
                }
            }
        }

        public void LoadApartments(ApartmentBooking[] apartmentBookings)
        {
            Apartments = new Dictionary<Guid, IList<Tuple<decimal, PageModel>>>();

            if (apartmentBookings != null && apartmentBookings.Length > 0)
            {
                foreach (var ab in apartmentBookings)
                {
                    var apartmentModel = _apartmentsCachedService.GetApartmentPageModel(ab.Apartment.Permalink.Name);
                    if (ab.Apartment.ParentId != null)
                    {
                        if (!Apartments.ContainsKey(ab.Apartment.ParentId.Value))
                        {
                            Apartments.Add(ab.Apartment.ParentId.Value, new List<Tuple<decimal, PageModel>>());
                        }

                        Apartments[ab.Apartment.ParentId.Value].Add(new Tuple<decimal, PageModel>(ab.Booking.Price, apartmentModel));
                    }
                }
            }
        }
    }
}