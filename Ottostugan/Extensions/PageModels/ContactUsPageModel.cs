using System.Collections.Generic;
using System.Linq;
using Ottostugan.DAL;
using Ottostugan.Models;
using Piranha.Models;

namespace Ottostugan.Extensions.PageModels
{
    public class ContactUsPageModel : PageModel
    {
        public string OrganisationName { get; set; }
        public string OrganisationEmail { get; set; }
        public string OrganisationPhone { get; set; }
        public string OrganisationStreetAddress { get; set; }
        public string OrganisationPostalCode { get; set; }
        public string OrganisationLocality { get; set; }

        public void Load()
        {
            IList<Preference> preferences;

            using (var db = new OttostuganContext())
            {
                preferences = db.Preferences.ToArray();
            }

            var organisationNamePreference = preferences.FirstOrDefault(p => p.Name == "Organisation Name");
            var organisationEmailPreference = preferences.FirstOrDefault(p => p.Name == "Organisation Email");
            var organisationPhonePreference = preferences.FirstOrDefault(p => p.Name == "Organisation Phone");
            var organisationStreetAddressPreference = preferences.FirstOrDefault(p => p.Name == "Organisation Street Address");
            var organisationPostalCodePreference = preferences.FirstOrDefault(p => p.Name == "Organisation Postal Code");
            var organisationLocalityPreference = preferences.FirstOrDefault(p => p.Name == "Organisation Locality");

            OrganisationName = organisationNamePreference == null
                ? string.Empty
                : organisationNamePreference.Value;

            OrganisationEmail = organisationEmailPreference == null
                ? string.Empty
                : organisationEmailPreference.Value.Replace("@", "//").Replace(".", "/"); ;

            OrganisationPhone = organisationPhonePreference == null
                ? string.Empty
                : organisationPhonePreference.Value;

            OrganisationStreetAddress = organisationStreetAddressPreference == null
                ? string.Empty
                : organisationStreetAddressPreference.Value;

            OrganisationPostalCode = organisationPostalCodePreference == null
                ? string.Empty
                : organisationPostalCodePreference.Value;

            OrganisationLocality = organisationLocalityPreference == null
                ? string.Empty
                : organisationLocalityPreference.Value;
        }
    }
}