﻿using Ottostugan.Extensions.Regions;
using Piranha.Models;

namespace Ottostugan.Extensions.PageModels
{
    public class StartPageModel : PageModel
    {
        public PageModel Content1 { get; set; }
        public PageModel Content2 { get; set; }
        public PageModel Content3 { get; set; }
        public PageModel Content4 { get; set; }
        public PageModel Content5 { get; set; }
        public PageModel Content6 { get; set; }

        public void LoadContentPages()
        {
            var region1 = Region<ContentPageRegion>("Content2");
            var region2 = Region<ContentPageRegion>("Content1");
            var region3 = Region<ContentPageRegion>("Content3");
            var region4 = Region<ContentPageRegion>("Content4");
            var region5 = Region<ContentPageRegion>("Content5");
            var region6 = Region<ContentPageRegion>("Content6");

            var contentPage1Id = (region1 == null) ? null : region1.Id;
            var contentPage2Id = (region2 == null) ? null : region2.Id;
            var contentPage3Id = (region3 == null) ? null : region3.Id;
            var contentPage4Id = (region4 == null) ? null : region4.Id;
            var contentPage5Id = (region5 == null) ? null : region5.Id;
            var contentPage6Id = (region6 == null) ? null : region6.Id;

            Content1 = contentPage1Id.HasValue ? GetById(contentPage1Id.Value) : null;
            Content2 = contentPage2Id.HasValue ? GetById(contentPage2Id.Value) : null;
            Content3 = contentPage3Id.HasValue ? GetById(contentPage3Id.Value) : null;
            Content4 = contentPage4Id.HasValue ? GetById(contentPage4Id.Value) : null;
            Content5 = contentPage5Id.HasValue ? GetById(contentPage5Id.Value) : null;
            Content6 = contentPage6Id.HasValue ? GetById(contentPage6Id.Value) : null;

        }

    }
}