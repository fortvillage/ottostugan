﻿using System;
using System.Linq;
using Ottostugan.Areas.Manager.Services;
using Ottostugan.DAL;
using Ottostugan.Extensions.Regions;
using Ottostugan.Models;
using Ottostugan.Services;
using Ottostugan.ViewModels;
using Piranha.Models;

namespace Ottostugan.Extensions.PageModels
{
    public class ApartmentPageModel : PageModel
    {
        private readonly IBookingService _service;
        private readonly IPriceListService _priceListService;
        private readonly IApartmentsCachedService _apartmentsCachedService;
        private readonly IHousePageCachedService _housePageCachedService;

        public const int SecondaryImagesCount = 8;

        public ApartmentPageModel()
        {
            _service = new BookingService();
            _priceListService = new PriceListService();
            _apartmentsCachedService = new ApartmentsCachedService();
            _housePageCachedService = new HousePageCachedService();
        }

        public PageModel House { get; set; }

        public PriceList PriceList { get; set; }

        public void LoadPriceList()
        {
            var region = Region<PriceListRegion>("PriceListId");
            var id = region.Id;
            if (id.HasValue)
            {
                PriceList = _priceListService.GetPriceListById(id.Value);
            }
        }

        public void LoadHousePage()
        {
            using (var db = new Piranha.DataContext())
            {
                var apartmentPage = _apartmentsCachedService.GetApartmentById(Page.Id);

                var housePage = apartmentPage.ParentId.HasValue ?_housePageCachedService.GetHousePageById(apartmentPage.ParentId.Value) : null;

                if (housePage != null)
                {
                    House = GetById(housePage.Id);
                }
            }
        }

        public SearchType SearchType { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }
}