﻿using System.ComponentModel.Composition;
using Piranha.Extend;

namespace Ottostugan.Extensions.PageTypes
{
    [Export(typeof(IPageType))]
    public class ContactUsPage : PageType
    {
        public ContactUsPage()
        {
            Name = "Contact Us Page";
            Description = "Normal page";
            Preview = "<table class=\"template\"><tr><td id=\"Content\"></td></tr></table>";
            Controller = "ContactUsPage";
            View = "ContactUsPage";
        }
    }
}