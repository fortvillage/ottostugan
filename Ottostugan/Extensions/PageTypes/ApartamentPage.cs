﻿using System.ComponentModel.Composition;
using Ottostugan.Extensions.Regions;
using Piranha.Extend;

namespace Ottostugan.Extensions.PageTypes
{
    [Export(typeof(IPageType))]
    public class ApartmentPage : PageType
    {
        public ApartmentPage()
        {
            Name = "Apartment Page";
            Description = "Apartment Page";
            Preview = "<table class=\"template\"><tr><td id=\"Content\"></td></tr></table>";
            Controller = "ApartmentPage";
            View = "ApartmentPage";


            Regions.Add(new RegionType()
            {
                InternalId = "ApartmentName",
                Name = "Apartament Name",
                Type = typeof(SimpleTextRegion)
            });

            Regions.Add(new RegionType()
            {
                InternalId = "ApartmentShortText",
                Name = "Apartament Short Text",
                Type = typeof(SimpleTextRegion)
            });

            Regions.Add(new RegionType()
            {
                InternalId = "ApartmentText",
                Name = "Apartament Text",
                Type = typeof(Piranha.Extend.Regions.HtmlRegion)
            });

            Regions.Add(new RegionType()
            {
                InternalId = "ApartmentMainImage",
                Name = "Apartment Image",
                Type = typeof(Piranha.Extend.Regions.ImageRegion)
            });

            Regions.Add(new RegionType()
            {
                InternalId = "ApartmentSecondaryImage1",
                Name = "Apartment Secondary Image 1",
                Type = typeof(Piranha.Extend.Regions.ImageRegion)
            });

            Regions.Add(new RegionType()
            {
                InternalId = "ApartmentSecondaryImage2",
                Name = "Apartment Secondary Image 2",
                Type = typeof(Piranha.Extend.Regions.ImageRegion)
            });

            Regions.Add(new RegionType()
            {
                InternalId = "ApartmentSecondaryImage3",
                Name = "Apartment Secondary Image 3",
                Type = typeof(Piranha.Extend.Regions.ImageRegion)
            });

            Regions.Add(new RegionType()
            {
                InternalId = "ApartmentSecondaryImage4",
                Name = "Apartment Secondary Image 4",
                Type = typeof(Piranha.Extend.Regions.ImageRegion)
            });

            Regions.Add(new RegionType()
            {
                InternalId = "ApartmentSecondaryImage5",
                Name = "Apartment Secondary Image 5",
                Type = typeof(Piranha.Extend.Regions.ImageRegion)
            });

            Regions.Add(new RegionType()
            {
                InternalId = "ApartmentSecondaryImage6",
                Name = "Apartment Secondary Image 6",
                Type = typeof(Piranha.Extend.Regions.ImageRegion)
            });

            Regions.Add(new RegionType()
            {
                InternalId = "ApartmentSecondaryImage7",
                Name = "Apartment Secondary Image 7",
                Type = typeof(Piranha.Extend.Regions.ImageRegion)
            });

            Regions.Add(new RegionType()
            {
                InternalId = "ApartmentSecondaryImage8",
                Name = "Apartment Secondary Image 8",
                Type = typeof(Piranha.Extend.Regions.ImageRegion)
            });

            Regions.Add(new RegionType()
            {
                InternalId = "PriceListId",
                Name = "Price List",
                Type = typeof(PriceListRegion)
            });

            Properties.Add("Size");
            Properties.Add("Adult beds");
            Properties.Add("Extra beds");
            Properties.Add("Sauna");
            Properties.Add("Meters to lift");
            Properties.Add("Rental Phone");
            Properties.Add("Rental Email");
        }


    }
}