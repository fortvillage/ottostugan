using System.ComponentModel.Composition;
using Piranha.Extend;

namespace Ottostugan.Extensions.PageTypes
{
    [Export(typeof(IPageType))]
    public class GuestBookPage : PageType
    {
        public GuestBookPage()
        {
            Name = "Guest Book Page";
            Description = "Guest Book Page";
            Preview = "<table class=\"template\"><tr><td id=\"Content\"></td></tr></table>";
            Controller = "GuestBookPage";
            View = "GuestBookPage";
        }
    }
}