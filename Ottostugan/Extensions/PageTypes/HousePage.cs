﻿using System.ComponentModel.Composition;
using Ottostugan.Extensions.Regions;
using Piranha.Extend;

namespace Ottostugan.Extensions.PageTypes
{
    [Export(typeof(IPageType))]
    public class HousePage : PageType
    {
        public HousePage()
        {
            Name = "House Page";
            Description = "House Page";
            Preview = "<table class=\"template\"><tr><td id=\"Content\"></td></tr></table>";
            Controller = "HousePage";
            View = "HousePage";

            Regions.Add(new RegionType()
            {
                InternalId = "HouseName",
                Name = "House Name",
                Type = typeof(SimpleTextRegion)
            });

            Regions.Add(new RegionType()
            {
                InternalId = "HouseMainImage",
                Name = "House Image",
                Type = typeof(Piranha.Extend.Regions.ImageRegion)
            });

            Regions.Add(new RegionType()
            {
                InternalId = "HouseText",
                Name = "House Text",
                Type = typeof(Piranha.Extend.Regions.HtmlRegion)
            });
        }
    }
}