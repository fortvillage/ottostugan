﻿using System.ComponentModel.Composition;
using Piranha.Extend;
using Piranha.Extend.Regions;

namespace Ottostugan.Extensions.PageTypes
{
    [Export(typeof(IPageType))]
    public class AboutUsPage : PageType
    {
        public AboutUsPage()
        {
            Name = "About Us Page";
            Description = "Normal page";
            Preview = "<table class=\"template\"><tr><td id=\"Content\"></td></tr></table>";
            Controller = "AboutUsPage";
            View = "AboutUsPage";

            Regions.Add(new RegionType()
            {
                InternalId = "AboutUsContent",
                Name = "About Us Content",
                Type = typeof (HtmlRegion)
            });
        }
    }
}