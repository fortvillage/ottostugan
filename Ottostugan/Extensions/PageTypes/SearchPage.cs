﻿using System.ComponentModel.Composition;
using Piranha.Extend;

namespace Ottostugan.Extensions.PageTypes
{
    [Export(typeof(IPageType))]
    public class SearchPage : PageType
    {
        public SearchPage()
        {
            Name = "Search Page";
            Description = "Search Page";
            Preview = "<table class=\"template\"><tr><td id=\"Content\"></td></tr></table>";
            Controller = "SearchPage";
            View = "SearchPage";
        }
    }
}