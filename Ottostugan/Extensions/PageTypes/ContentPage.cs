﻿using System.ComponentModel.Composition;
using Ottostugan.Extensions.Regions;
using Piranha.Extend;

namespace Ottostugan.Extensions.PageTypes
{
    [Export(typeof (IPageType))]
    public class ContentPage : PageType
    {
        public ContentPage()
        {
            Name = "Content Page";
            Description = "Normal page";
            Preview = "<table class=\"template\"><tr><td id=\"Content\"></td></tr></table>";
            Controller = "ContentPage";
            View = "ContentPage";

            Regions.Add(new RegionType()
            {
                InternalId = "Title",
                Name = "Content Page Title",
                Type = typeof (PageTitleRegion)
            });

            Regions.Add(new RegionType()
            {
                InternalId = "Content",
                Name = "Content Page Body",
                Type = typeof (Piranha.Extend.Regions.HtmlRegion)
            });
            
            Regions.Add(new RegionType()
            {
                InternalId = "StartPageImage",
                Name = "Start Page image",
                Type = typeof (Piranha.Extend.Regions.ImageRegion)
            });

            Regions.Add(new RegionType()
            {
                InternalId = "StartPageContent",
                Name = "Content For Start Page",
                Type = typeof (Piranha.Extend.Regions.TextRegion)
            });

            Regions.Add(new RegionType()
            {
                InternalId = "SocialMediaLink1",
                Name = "Link 1",
                Type = typeof (SocialMediaLinkRegion)
            });

            Regions.Add(new RegionType()
            {
                InternalId = "SocialMediaLink2",
                Name = "Link 2",
                Type = typeof (SocialMediaLinkRegion)
            });

            Regions.Add(new RegionType()
            {
                InternalId = "SocialMediaLink3",
                Name = "Link 3",
                Type = typeof (SocialMediaLinkRegion)
            });
        }
    }
}