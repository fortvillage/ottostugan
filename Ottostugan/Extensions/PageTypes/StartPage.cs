﻿using System.ComponentModel.Composition;
using Ottostugan.Extensions.Regions;
using Piranha.Extend;

namespace Ottostugan.Extensions.PageTypes
{
    [Export(typeof(IPageType))]
    public class StartPage : PageType
    {
        public StartPage()
        {
            Name = "Start Page";
            Description = "Web Site Start Page";
            Preview = "<table class=\"template\"><tr><td id=\"Content\"></td></tr></table>";
            Controller = "StartPage";
            View = "StartPage";

            Regions.Add(new RegionType()
            {
                InternalId = "Title",
                Name = "Start Page Title",
                Type = typeof(PageTitleRegion)
            });

            Regions.Add(new RegionType()
            {
                InternalId = "MainImage",
                Name = "Start Page Image",
                Type = typeof(Piranha.Extend.Regions.ImageRegion)
            });

            Regions.Add(new RegionType()
            {
                InternalId = "Content1",
                Name = "Content Page 1",
                Type = typeof(ContentPageRegion)
            });

            Regions.Add(new RegionType()
            {
                InternalId = "Content2",
                Name = "Content Page 2",
                Type = typeof(ContentPageRegion)
            });

            Regions.Add(new RegionType()
            {
                InternalId = "Content3",
                Name = "Content Page 3",
                Type = typeof(ContentPageRegion)
            });

            Regions.Add(new RegionType()
            {
                InternalId = "Content4",
                Name = "Content Page 4",
                Type = typeof(ContentPageRegion)
            });

            Regions.Add(new RegionType()
            {
                InternalId = "Content5",
                Name = "Content Page 5",
                Type = typeof(ContentPageRegion)
            });

            Regions.Add(new RegionType()
            {
                InternalId = "Content6",
                Name = "Content Page 6",
                Type = typeof(ContentPageRegion)
            });
        }
    }
}

