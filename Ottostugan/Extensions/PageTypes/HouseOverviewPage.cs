﻿using System.ComponentModel.Composition;
using Piranha.Extend;

namespace Ottostugan.Extensions.PageTypes
{
    [Export(typeof(IPageType))]
    public class HouseOverviewPage : PageType
    {
        public HouseOverviewPage()
        {
            Name = "Houses Overview Page";
            Description = "Houses Overview Page";
            Preview = "<table class=\"template\"><tr><td id=\"Content\"></td></tr></table>";
            Controller = "HouseOverviewPage";
            View = "HouseOverviewPage";
        }
    }
}