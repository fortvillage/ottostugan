﻿using System;
using System.Web;

namespace Ottostugan.Caching
{
    public interface ICacheService
    {
        T Get<T>(string cacheId, Func<T> getItemCallback) where T : class;
    }

    public class InMemoryCache : ICacheService
    {
        public T Get<T>(string cacheId, Func<T> getItemCallback) where T : class
        {
            T item = HttpRuntime.Cache.Get(cacheId) as T;
            if (item == null)
            {
                item = getItemCallback();
                HttpContext.Current.Cache.Insert(cacheId, item, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromSeconds(30));
            }
            return item;
        }
    }
}