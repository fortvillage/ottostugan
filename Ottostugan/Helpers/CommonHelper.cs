﻿using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Web.Hosting;
using Ottostugan.Entities;
using Ottostugan.Models;

namespace Ottostugan.Helpers
{
    public static class CommonHelper
    {
        public static DateTime ConvertFromUnixTimestamp(this double timestamp)
        {
            var origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return origin.AddSeconds(timestamp);
        }

        public static decimal NormalizeDecimal(this decimal value)
        {
            return value / 1.000000000000000000000000000000000m;
        }

        public static BookingEntity ToBookingEntity(this Booking booking)
        {
            return new BookingEntity()
            {
                ApartmentId = booking.ApartmentId,
                BookedByEmail = booking.BookedByEmail,
                BookedByName = booking.BookedByName,
                BookedByPhone = booking.BookedByPhone,
                Comments = booking.Comments,
                CreatedDate = booking.CreatedDate,
                FromDate = booking.FromDate,
                Id = booking.Id,
                Price = 0,
                Status = booking.Status,
                ToDate = booking.ToDate,
                IsPaidDeposit = booking.IsPaidDeposit,
                IsPaidFully = booking.IsPaidFully
            };
        }

        public static string FormatPrice(decimal price)
        {
            return string.Format("{0} {1}", price.NormalizeDecimal().ToString("# ##0.##"),
                ConfigurationManager.AppSettings["CurrencySymbol"]);
        }
    }

    public static class GlobalizeUrls
    {

        public static string Globalize { get { return "~/Scripts/globalize/globalize.js"; } }

        public static string GlobalizeCulture
        {
            get
            {
                var currentCulture = CultureInfo.CurrentCulture;
                const string filePattern = "~/scripts/globalize/cultures/globalize.culture.{0}.js";
                var regionalisedFileToUse = string.Format(filePattern, "en-GB");

                if (File.Exists(HostingEnvironment.MapPath(string.Format(filePattern, currentCulture.Name))))
                    regionalisedFileToUse = string.Format(filePattern, currentCulture.Name);
                else if (File.Exists(HostingEnvironment.MapPath(string.Format(filePattern, currentCulture.TwoLetterISOLanguageName))))
                    regionalisedFileToUse = string.Format(filePattern, currentCulture.TwoLetterISOLanguageName);

                return regionalisedFileToUse;
            }
        }
    }
}