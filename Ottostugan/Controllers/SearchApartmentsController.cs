﻿using System.Web.Mvc;
using Ottostugan.Extensions.PageModels;
using Ottostugan.Helpers;
using Ottostugan.Models;
using Ottostugan.Services;

namespace Ottostugan.Controllers
{
    public class SearchApartmentsController : Controller
    {
        private readonly IBookingService _service;

        public SearchApartmentsController()
        {
            _service = new BookingService();
        }

        //
        // GET: /Search/

        public ActionResult Search(int? adults, int? children, double start, double end, BookingStatus? status, SearchType? searchType)
        {
            var model = new SearchPageModel();
            model.LoadHouses();
            var startDate = (start / 1000).ConvertFromUnixTimestamp();
            var endDate = (end / 1000).ConvertFromUnixTimestamp();
            var apartments = _service.SearchApartments(adults, children, startDate, endDate, status, searchType);
            model.LoadApartments(apartments);
            return PartialView("SearchApartments", model);
        }

    }
}
