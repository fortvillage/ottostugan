﻿using System;
using System.Text;
using System.Web.Mvc;
using Newtonsoft.Json;
using Ottostugan.Helpers;
using Ottostugan.Models;
using Ottostugan.Services;

namespace Ottostugan.Controllers
{
    public class BookingController : Controller
    {
        private readonly IBookingService _bookingService;

        public BookingController()
        {
            _bookingService = new BookingService();
        }

        [HttpGet]
        public ActionResult SearchBookings(int? adults, int? children, double start, double end, SearchType? searchType, string fromDate = "", string toDate = "")
        {
            var startDate = start.ConvertFromUnixTimestamp();
            var endDate = end.ConvertFromUnixTimestamp();

            DateTime fromDateTime = new DateTime(); ;
            DateTime toDateTime = new DateTime(); ;
            if (DateTime.TryParse(fromDate, out fromDateTime) && DateTime.TryParse(toDate, out toDateTime))
            {

            }

            return Content(Newtonsoft.Json.JsonConvert.SerializeObject(_bookingService.SearchBookings(adults, children, searchType, startDate, endDate, fromDateTime, toDateTime)));
        }

        [HttpGet]
        public ActionResult SearchBookingsForApartment(Guid apartmentId, double start, double end,
            SearchType? searchType, string fromDate = "", string toDate = "")
        {
            var startDate = start.ConvertFromUnixTimestamp();
            var endDate = end.ConvertFromUnixTimestamp();

            DateTime fromDateTime = new DateTime(); ;
            DateTime toDateTime = new DateTime(); ;
            if (DateTime.TryParse(fromDate, out fromDateTime) && DateTime.TryParse(toDate, out toDateTime))
            {
                
            }
            
            return Content(Newtonsoft.Json.JsonConvert.SerializeObject(_bookingService.GetBookingsByApartmentId(apartmentId, searchType, startDate, endDate, fromDateTime, toDateTime)));
        }

        
    }
}
