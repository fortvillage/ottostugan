using System.Web.Mvc;
using Ottostugan.Extensions.PageModels;
using Piranha.Mvc;

namespace Ottostugan.Controllers
{
    public class ContactUsPageController : SinglePageController
    {
        public ActionResult Index()
        {
            var model = GetModel<ContactUsPageModel>();
            model.Load();
            return View(model.GetView(), model);
        }
    }
}