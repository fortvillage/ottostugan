using System.Web.Mvc;
using Ottostugan.Extensions.PageModels;
using Piranha.Mvc;

namespace Ottostugan.Controllers
{
    public class HousePageController : SinglePageController
    {
        public ActionResult HousePage(string permalink)
        {
            var model = GetModel<HousePageModel>(permalink);
            model.LoadApartments();
            return PartialView(model.GetView(), model);
        }
    }
}