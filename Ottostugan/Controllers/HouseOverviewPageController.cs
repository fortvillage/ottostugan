﻿using System;
using System.Web.Mvc;
using Ottostugan.Extensions.PageModels;
using Piranha.Mvc;

namespace Ottostugan.Controllers
{
    public class HouseOverviewPageController : SinglePageController
    {
        public ActionResult Index()
        {
            var model = GetModel<HouseOverviewPageModel>();
            model.LoadHouses();
            model.LoadSearchPage();
            return View(model.GetView(), model);
        }
    }
}
