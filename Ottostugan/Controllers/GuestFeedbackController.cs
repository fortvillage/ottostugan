﻿using System;
using System.Web.Mvc;
using Ottostugan.Models;
using Ottostugan.Resources;
using Ottostugan.Services;

namespace Ottostugan.Controllers
{
    public class GuestFeedbackController : Controller
    {
        private readonly IGuestBookService _service;
        private readonly ISendEmailService _emailService;

        public GuestFeedbackController()
            : base()
        {
            _emailService = new SendEmailService();
            _service = new GuestBookService();
        }

        [HttpGet]
        public ActionResult PostFeedback(bool isSuccess = false)
        {
            var model = new GuestFeedbackViewModel {IsSuccess = isSuccess};
            return PartialView("PostFeedback", model);
        }

        [HttpPost]
        public ActionResult PostFeedback(GuestFeedbackViewModel model)
        {
            //if (ModelState.IsValid)
            //{
            //    _service.SaveFeedback(model);

            //    var subject = Labels.GuestbookEmailSubject;
            //    var bodyTemplate = Labels.GuestbookEmailBody;
            //    var body = string.Format(bodyTemplate, model.From, model.Comment);

            //    _emailService.SendEmail(subject, body);

            //    return RedirectToAction("PostFeedback", new { isSuccess = true});
            //}
            model.IsSuccess = false;
            return PartialView("PostFeedback", model);
        }
    }
}