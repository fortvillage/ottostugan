using System.Web.Mvc;
using Ottostugan.Extensions.PageModels;
using Piranha.Mvc;

namespace Ottostugan.Controllers
{
    public class StartPageController : SinglePageController
    {
        public ActionResult Index()
        {
            var model = GetModel<StartPageModel>();
            model.LoadContentPages();
            return View(model.GetView(), model);
        }
    }
}