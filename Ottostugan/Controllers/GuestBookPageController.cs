﻿using System.Web.Mvc;
using Ottostugan.Extensions.PageModels;
using Ottostugan.Services;
using Piranha.Mvc;

namespace Ottostugan.Controllers
{
    public class GuestBookPageController : SinglePageController
    {
        private readonly IGuestBookService _service;

        public GuestBookPageController() : base()
        {
            _service = new GuestBookService();
        }

        public ActionResult Index()
        {
            var model = GetModel<GuestBookPageModel>();
            model.Feedbacks = _service.GetPublishedFeedbacks();
            return View(model.GetView(), model);
        }
    }
}