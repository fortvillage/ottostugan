using System.Web.Mvc;
using Ottostugan.Extensions.PageModels;
using Piranha.Mvc;

namespace Ottostugan.Controllers
{
    public class ContentPageController : SinglePageController
    {
        public ActionResult Index()
        {
            var model = GetModel<ContentPageModel>();
            model.InitProperties();
            return View(model.GetView(), model);
        }
    }


}