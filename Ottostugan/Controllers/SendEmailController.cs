﻿using System;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using Ottostugan.Helpers;
using Ottostugan.Resources;
using Ottostugan.Services;
using Ottostugan.ViewModels;
using Piranha;
using Piranha.Entities;

namespace Ottostugan.Controllers
{
    public class SendEmailController : Controller
    {
        private readonly ISendEmailService _service;

        public SendEmailController()
        {
            _service = new SendEmailService();
        }

        public ActionResult SendBookingRequest(BookingRequestModel model)
        {
            var dateFormat = ConfigurationManager.AppSettings["DateFormat"];
            Page apartment;

            using (var db = new DataContext())
            {
                apartment = db.Pages.FirstOrDefault(p => p.Id == model.ApartmentId);
            }

            if (apartment == null)
            {
                throw new ArgumentException();
            }

            var subject = string.Format(Labels.BookingRequestSubject, model.FromDate.ToString(dateFormat), model.ToDate.ToString(dateFormat));
            var body = string.Format(Labels.BookingRequestBody, model.FromDate.ToString(dateFormat), model.ToDate.ToString(dateFormat), apartment.Title, model.Name, model.Phone, model.Email, model.Comments, model.Price < 0 ? "Prisförfrågan" : CommonHelper.FormatPrice(model.Price));

            _service.SendEmail(subject, body);

            return PartialView("BookingRequestSended");
        }
    }
}
