﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Ottostugan.DAL;
using Ottostugan.Models;
using Ottostugan.ViewModels.Main;

namespace Ottostugan.Controllers
{
    public class MainController : Controller
    {
        //
        // GET: /Main/

        public ActionResult Footer()
        {

            IList<Preference> preferences;

            using (var db = new OttostuganContext())
            {
                preferences = db.Preferences.ToArray();
            }

            var model = new FooterModel();

            var organisationNamePreference = preferences.FirstOrDefault(p => p.Name == "Organisation Name");
            var organisationEmailPreference = preferences.FirstOrDefault(p => p.Name == "Organisation Email");
            var organisationPhonePreference = preferences.FirstOrDefault(p => p.Name == "Organisation Phone");
            var organisationStreetAddressPreference = preferences.FirstOrDefault(p => p.Name == "Organisation Street Address");
            var organisationPostalCodePreference = preferences.FirstOrDefault(p => p.Name == "Organisation Postal Code");
            var organisationLocalityPreference = preferences.FirstOrDefault(p => p.Name == "Organisation Locality");

            model.OrganisationName = organisationNamePreference == null
                ? string.Empty
                : organisationNamePreference.Value;

            model.OrganisationEmail = organisationEmailPreference == null
                ? string.Empty
                : organisationEmailPreference.Value.Replace("@", "//").Replace(".", "/");

            model.OrganisationPhone = organisationPhonePreference == null
                ? string.Empty
                : organisationPhonePreference.Value;

            model.OrganisationStreetAddress = organisationStreetAddressPreference == null
                ? string.Empty
                : organisationStreetAddressPreference.Value;

            model.OrganisationPostalCode = organisationPostalCodePreference == null
                ? string.Empty
                : organisationPostalCodePreference.Value;

            model.OrganisationLocality = organisationLocalityPreference == null
                ? string.Empty
                : organisationLocalityPreference.Value;

            return PartialView("Partials/Footer", model);
        }

    }
}
