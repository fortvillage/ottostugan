using System.Web.Mvc;
using Ottostugan.Extensions.PageModels;
using Piranha.Mvc;

namespace Ottostugan.Controllers
{
    public class AboutUsPageController : SinglePageController
    {
        public ActionResult Index()
        {
            var model = GetModel<AboutUsPageModel>();
            return View(model.GetView(), model);
        }
    }
}