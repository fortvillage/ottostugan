﻿using System.Web.Mvc;
using Ottostugan.Extensions.PageModels;
using Piranha.Mvc;

namespace Ottostugan.Controllers
{
    public class SearchPageController : SinglePageController
    {
       public ActionResult Index()
        {
            var model = GetModel<SearchPageModel>();
            model.LoadHouses();
            return View(model.GetView(), model);
        }
    }
}