using System;
using System.Web.Mvc;
using Ottostugan.Extensions.PageModels;
using Ottostugan.Models;
using Piranha.Mvc;

namespace Ottostugan.Controllers
{
    public class ApartmentPageController : SinglePageController
    {
        public ActionResult Index(DateTime? start, DateTime? end, DateTime? fromDate, DateTime? toDate, SearchType? searchType)
        {
            var model = GetModel<ApartmentPageModel>();
            model.SearchType = searchType.HasValue ? searchType.Value : SearchType.SplittedWeek;
            model.LoadPriceList();
            model.LoadHousePage();
            model.StartDate = start;
            model.EndDate = end;
            return View(model.GetView(), model);
        }
    }
}