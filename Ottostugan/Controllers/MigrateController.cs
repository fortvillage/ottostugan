﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ottostugan.Migrations;

namespace Ottostugan.Controllers
{
    public class MigrateController : Controller
    {
        //
        // GET: /Migrate/

        
        public JsonResult Index(string what = "")
        {
            if(what == "migratelatest")
            {
                var mm = new Configuration.ManualMigration();
                mm.Update();
                return Json(true, JsonRequestBehavior.AllowGet);
            }

            return Json("404", JsonRequestBehavior.AllowGet);
        }

    }
}
