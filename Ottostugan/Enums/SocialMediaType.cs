﻿namespace Ottostugan.Enums
{
    public enum SocialMediaType
    {
        Facebook = 1,
        Twitter = 2,
        GooglePlus = 3
    }
}