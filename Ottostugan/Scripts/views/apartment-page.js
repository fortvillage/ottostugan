﻿window.onEventAfterAllRender = function (view) {
    if (window.selectedEventStartDate && window.selectedEventEndDate) {
        var events = $('.calendar').fullCalendar('clientEvents');
        var selectedEvent = null;
        $.each(events, function(index, event) {
            if (event.canBeSelected && jQuery.datepicker.formatDate('yy-mm-dd', event._start) == window.selectedEventStartDate && jQuery.datepicker.formatDate('yy-mm-dd', event._end) == window.selectedEventEndDate) {
                selectedEvent = event;
                return false;
            }
            return true;
        });
        
        if (selectedEvent) {
            window.onCalendarEventClick(selectedEvent);
        }
    }

};


$(function () {
    if (window.selectedEventStartDate && window.selectedEventEndDate) {
        $('.calendar').fullCalendar('gotoDate', window.startYear, window.startMonth);
    }
});