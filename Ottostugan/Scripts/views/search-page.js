﻿$(document).ready(function () {
    $("#datepickerFrom").datepicker({ dateFormat: 'yy-mm-dd' });
    $("#datepickerTo").datepicker({ dateFormat: 'yy-mm-dd' });

    $('#datepickerFrom').change(function () {
        customDateChanged()
    });

    $('#datepickerTo').change(function () {
        customDateChanged();
    });

    function customDateChanged() {
        if ($("#datepickerFrom").val() == "" || $("#datepickerTo").val() == "") {

            return;
        } else {

        }
        searchBookings();
    }

});

window.searchBookings = function () {
    $('#searchResultsContainer').html('');
    window.events.data.adults = $('#ddlAdultsCount').val();
    window.events.data.searchType = getSearchType();

    if (getSearchType() == "ChooseFreely") {
        window.events.data.fromDate = $("#datepickerFrom").val();
        window.events.data.toDate = $("#datepickerTo").val();
    } else {
        window.events.data.fromDate = "";
        window.events.data.toDate = "";
    }

    window.isEventSourceRemoved = true;
    $('.calendar').fullCalendar('removeEventSource', window.events);
    window.isEventSourceRemoved = false;
    $('.calendar').fullCalendar('addEventSource', window.events);
};

function getSearchType() {
    var fullWeekSearchBtn = document.getElementById("rbtnFullWeek");
    var splittedWeekSearchBtn = document.getElementById("rbtnSplittedWeek");
    var chooseFreelySearchBtn = document.getElementById("rbtnChooseFreely");
    if (chooseFreelySearchBtn.checked) {
        $('.chooseFreelyDatepickers').show();
        return chooseFreelySearchBtn.value
    }
    $('.chooseFreelyDatepickers').hide();

    return fullWeekSearchBtn.checked ? fullWeekSearchBtn.value : splittedWeekSearchBtn.value;
}

function updateSelectedBookingInfo(calEvent) {
    var $bookingInfo = $('#selectedBookingInfo');
    if (calEvent) {
        $bookingInfo.find('#startDate').text($.fullCalendar.formatDate(calEvent.start, window.dateFormat));
        $bookingInfo.find('#endDate').text($.fullCalendar.formatDate(calEvent.end, window.dateFormat));
        $bookingInfo.show();
        $('.selected-event').removeClass('selected-event');
        $('.' + calEvent._id).addClass('selected-event');
    } else {
        $bookingInfo.hide();
    }
}

window.onCalendarEventClick = function (calEvent, jsEvent, view) {
    window.searchApartments(calEvent);
};

window.searchApartments = function (calEvent) {
    updateSelectedBookingInfo(calEvent);
    $.get(window.searchUrl, {
        adults: $('#ddlAdultsCount').val(),
        start: calEvent ? calEvent.start.getTime() : new Date().getTime(),
        end: calEvent ? calEvent.end.getTime() : new Date().getTime(),
        status: 'Available',
        searchType: getSearchType()
    }, function (html) {
        $('#searchResultsContainer').html(html);
        window.Default.utils.miscellaneous();
        $('[data-apartment-id] p.link a').on('click', function () {
            var $bookingInfo = $('#selectedBookingInfo');
            $(this).attr("href", this.href + "?start=" + $bookingInfo.find('#startDate').text() + "&end=" + $bookingInfo.find('#endDate').text() + "&fromDate=" + $bookingInfo.find('#startDate').text() + "&toDate=" + $bookingInfo.find('#endDate').text() + "&searchType=" + getSearchType());
        });
    });
};