using System;
using System.ComponentModel.DataAnnotations;

namespace Ottostugan.Models
{
    public class GuestFeedbackViewModel
    {
        [Required(ErrorMessageResourceType = typeof (Resources.Extensions), ErrorMessageResourceName = "GuestFeedbackViewModel_From_Please_fill_out_this_field")]
        public string From { get; set; }

        [Required(ErrorMessageResourceType = typeof (Resources.Extensions), ErrorMessageResourceName = "GuestFeedbackViewModel_Comment_Please_fill_out_this_field")]
        public string Comment { get; set; }

        public bool IsSuccess { get; set; }
    }
}