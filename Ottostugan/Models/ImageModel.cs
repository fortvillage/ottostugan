﻿using System;

namespace Ottostugan.Models
{
    public class ImageModel
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
    }
}