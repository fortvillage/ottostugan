﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ottostugan.Models
{
    public class Preference
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof (Resources.Extensions), ErrorMessageResourceName = "Preference_Name__Name__field_is_required")]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof (Resources.Extensions), ErrorMessageResourceName = "Preference_Value__Value__field_is_required")]
        public string Value { get; set; }
    }
}