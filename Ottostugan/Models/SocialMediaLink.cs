﻿using System.ComponentModel.DataAnnotations;

namespace Ottostugan.Models
{
    public class SocialMediaLink
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof (Resources.Extensions), ErrorMessageResourceName = "SocialMediaLink_Name_Field_is_required")]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof (Resources.Extensions), ErrorMessageResourceName = "SocialMediaLink_Value_Field_is_required")]
        public string Value { get; set; }

        public int? Type { get; set; }
    }
}