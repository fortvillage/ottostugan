namespace Ottostugan.Models
{
    public enum BookingStatus : byte
    {
        Available = 0,
        NotAvalable = 1,
        Booked = 2
    }
}