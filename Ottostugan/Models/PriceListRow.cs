﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ottostugan.Models
{
    public class PriceListRow
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("PriceList")]
        public int PriceListId { get; set; }

        public PriceList PriceList { get; set; }

        public double PriceFullWeek { get; set; }

        public double PriceSplittedSunThurs { get; set; }

        public double PriceSplittedThursSun { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }
    }
}