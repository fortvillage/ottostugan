using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Ottostugan.Models
{
    public class Booking
    {
        [Key]
        public int Id { get; set; }

        public Guid ApartmentId { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        [NotMapped]
        public List<DateTime> Dates
        {
            get
            {
                if (ToDate < FromDate) //Bad data
                    return new List<DateTime>();


                var list = new List<DateTime>();
                var iDate = FromDate;

                while (iDate <= ToDate)
                {
                    list.Add(iDate.Date);
                    iDate = iDate.AddDays(1);
                }

                return list;
            }
        }

        public BookingStatus Status { get; set; }

        public string BookedByName { get; set; }

        public string BookedByEmail { get; set; }

        public string BookedByPhone { get; set; }

        public string Comments { get; set; }

        public DateTime CreatedDate { get; set; }

        public bool IsPaidDeposit { get; set; }

        public bool IsPaidFully { get; set; }

        public decimal Price { get; set; }
    }
}