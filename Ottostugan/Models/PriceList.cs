﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ottostugan.Models
{
    public class PriceList
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public IList<PriceListRow> PriceListRows { get; set; }
    }
}