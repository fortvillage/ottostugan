﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Ottostugan.Models
{
    public class GuestFeedback
    {
        [Key]
        public int Id { get; set; }

        public DateTime CreatedDate { get; set; }

        public bool Published { get; set; }

        public string From { get; set; }

        public string Comment { get; set; }
    }
}