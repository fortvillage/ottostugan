﻿using Ottostugan.Entities;
using Piranha.Entities;

namespace Ottostugan.Models
{
    public class ApartmentBooking
    {
        public Page Apartment { get; set; }
        public BookingEntity Booking { get; set; }
    }
}