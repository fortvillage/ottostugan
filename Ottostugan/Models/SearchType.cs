namespace Ottostugan.Models
{
    public enum SearchType : byte
    {
        FullWeek = 0,
        SplittedWeek = 1,
        ChooseFreely = 2
    }
}