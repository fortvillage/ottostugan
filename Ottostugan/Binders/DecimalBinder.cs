﻿using System;
using System.Globalization;
using System.Web.Mvc;

namespace Ottostugan.Binders
{
    public class DecimalModelBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var valueProviderResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);

            var separator = NumberFormatInfo.CurrentInfo.NumberDecimalSeparator;

            if (valueProviderResult == null)
                return base.BindModel(controllerContext, bindingContext);

            var value = valueProviderResult.AttemptedValue.Replace(".", separator).Replace(",", separator);
            
            return Convert.ToDecimal(value);
        }
    }
}