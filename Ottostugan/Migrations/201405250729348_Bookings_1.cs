namespace Ottostugan.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Bookings_1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Preference", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Preference", "Value", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Preference", "Value", c => c.String());
            AlterColumn("dbo.Preference", "Name", c => c.String());
        }
    }
}
