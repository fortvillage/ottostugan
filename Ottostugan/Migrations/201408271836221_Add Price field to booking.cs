namespace Ottostugan.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPricefieldtobooking : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Booking", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Booking", "Price");
        }
    }
}
