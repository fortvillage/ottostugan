namespace Ottostugan.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SocialMediaLinkType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SocialMediaLink", "Type", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SocialMediaLink", "Type");
        }
    }
}
