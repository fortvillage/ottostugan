namespace Ottostugan.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SocialMediaLinks : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SocialMediaLink",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Value = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SocialMediaLink");
        }
    }
}
