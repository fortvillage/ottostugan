namespace Ottostugan.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IsPaidDeposit_IsPaidFully_For_Booking : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Booking", "IsPaidDeposit", c => c.Boolean(nullable: false));
            AddColumn("dbo.Booking", "IsPaidFully", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Booking", "IsPaidFully");
            DropColumn("dbo.Booking", "IsPaidDeposit");
        }
    }
}
