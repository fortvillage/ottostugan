namespace Ottostugan.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SitePreferences : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Preference",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Value = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Preference");
        }
    }
}
