namespace Ottostugan.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PriceListRows2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PriceListRow",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PriceListId = c.Int(nullable: false),
                        PriceFullWeek = c.Double(nullable: false),
                        PriceSplittedSunThurs = c.Double(nullable: false),
                        PriceSplittedThursSun = c.Double(nullable: false),
                        FromDate = c.DateTime(nullable: false),
                        ToDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PriceList", t => t.PriceListId, cascadeDelete: true)
                .Index(t => t.PriceListId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PriceListRow", "PriceListId", "dbo.PriceList");
            DropIndex("dbo.PriceListRow", new[] { "PriceListId" });
            DropTable("dbo.PriceListRow");
        }
    }
}
