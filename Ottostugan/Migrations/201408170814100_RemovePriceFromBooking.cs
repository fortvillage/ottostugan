namespace Ottostugan.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovePriceFromBooking : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Booking", "Price");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Booking", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
