namespace Ottostugan.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Ottostugan.DAL.OttostuganContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Ottostugan.DAL.OttostuganContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }


        public class ManualMigration
        {
            public void Update(string targetMigration = "")
            {
                var configuration = new Configuration();
                var migrator = new DbMigrator(configuration);

                //Update database to latest migration
                if(string.IsNullOrEmpty(targetMigration))
                    migrator.Update();
                else
                    migrator.Update(targetMigration);
            }
        }
    }
}
