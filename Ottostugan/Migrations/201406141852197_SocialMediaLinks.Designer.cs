// <auto-generated />
namespace Ottostugan.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.0.2-21211")]
    public sealed partial class SocialMediaLinks : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(SocialMediaLinks));
        
        string IMigrationMetadata.Id
        {
            get { return "201406141852197_SocialMediaLinks"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
