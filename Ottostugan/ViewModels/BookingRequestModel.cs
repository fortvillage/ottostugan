﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Ottostugan.ViewModels
{
    public class BookingRequestModel
    {

        [Required]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime FromDate { get; set; }

        [Required]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ToDate { get; set; }

        [Required]
        public Guid ApartmentId { get; set; }

        [Required]
        public decimal Price { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Labels), ErrorMessageResourceName = "BookingRequestModel_Name_Please_fill_out_this_field")]
        [Display(ResourceType = typeof(Resources.Labels), Name = "BookingRequestModel_Name_Enter_your_name")]
        public string Name { get; set; }

        [Display(ResourceType = typeof(Resources.Labels), Name = "BookingRequestModel_Phone_Enter_your_phone")]
        public string Phone { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Labels), ErrorMessageResourceName = "BookingRequestModel_Email_Please_fill_out_this_field")]
        [Display(ResourceType = typeof(Resources.Labels), Name = "BookingRequestModel_Email_Enter_your_email_address")]
        [EmailAddress(ErrorMessage = null, ErrorMessageResourceType = typeof(Resources.Labels), ErrorMessageResourceName = "BookingRequestModel_Email_Please_enter_valid_email")]
        public string Email { get; set; }

        [Display(ResourceType = typeof(Resources.Labels), Name = "BookingRequestModel_Comments_Enter_your_comments")]
        public string Comments { get; set; }
    }
}