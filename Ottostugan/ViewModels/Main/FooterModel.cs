﻿namespace Ottostugan.ViewModels.Main
{
    public class FooterModel
    {
        public string OrganisationName { get; set; }
        public string OrganisationEmail { get; set; }
        public string OrganisationPhone { get; set; }
        public string OrganisationStreetAddress { get; set; }
        public string OrganisationPostalCode { get; set; }
        public string OrganisationLocality { get; set; }
    }
}