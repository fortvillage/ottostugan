using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Ottostugan.Models;

namespace Ottostugan.ViewModels
{
    [DataContract]
    public class BookingCalendarEvent
    {
        [DataMember(Name = "title")]
        public string Title { get; set; }
        [DataMember(Name = "bookingId")]
        public int BookingId { get;set; }
        [DataMember(Name = "start")]
        public string Start { get; set; }
        [DataMember(Name = "end")]
        public string End { get; set; }
        [DataMember(Name = "color")]
        public string Color { get; set; }
        [DataMember(Name = "textColor")]
        public string TextColor { get; set; }
        [DataMember(Name = "allDay")]
        public bool AllDay { get; set; }
        [DataMember(Name = "canBeSelected")]
        public bool CanBeSelected { get; set; }
        [DataMember(Name = "price")]
        public decimal Price { get; set; }
        [DataMember(Name = "formattedPrice")]
        public string FormattedPrice { get; set; }
    }
}