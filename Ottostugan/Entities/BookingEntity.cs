﻿using System;
using Ottostugan.Models;

namespace Ottostugan.Entities
{
    public class BookingEntity
    {
        public int Id { get; set; }

        public Guid ApartmentId { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public Decimal Price { get; set; }

        public BookingStatus Status { get; set; }

        public string BookedByName { get; set; }

        public string BookedByEmail { get; set; }

        public string BookedByPhone { get; set; }

        public string Comments { get; set; }

        public DateTime CreatedDate { get; set; }

        public bool IsPaidDeposit { get; set; }

        public bool IsPaidFully { get; set; }
    }
}