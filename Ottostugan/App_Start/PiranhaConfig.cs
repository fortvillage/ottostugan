﻿using System;
using System.Linq;
using System.Web;
using Piranha;
using Piranha.Models;
using Piranha.WebPages;

namespace Ottostugan.App_Start
{
    public static class PiranhaConfig
    {
        public static void Register()
        {
            Config.PrefixlessPermalinks = true;

            Piranha.WebPages.Hooks.Menu.RenderLevelStart = (ui, str, cssclass) => str.AppendLine("<ul>");

            Piranha.WebPages.Hooks.Menu.RenderLevelEnd = (ui, str, cssclass) => str.AppendLine("</ul>");

            Piranha.WebPages.Hooks.Menu.RenderItemStart = 
                (ui, str, page, active, activechild) =>
                {
                    var activeClass = IsStartPage(page) ? "a active" : "active";
                    var regularClass = IsStartPage(page) ? "a" : "";

                    str.AppendFormat("<li class=\"{0}\">", active || activechild ? activeClass : regularClass);
                };

            Piranha.WebPages.Hooks.Menu.RenderItemLink = 
                (ui, str, page) => str.AppendLine(String.Format("<a accesskey=\"{2}\" href=\"{0}\">{1}</a> <em>{2}</em>", GenerateUrl(page), !String.IsNullOrEmpty(page.NavigationTitle) ? page.NavigationTitle : page.Title, page.Seqno));

            Piranha.WebPages.Hooks.Menu.RenderItemEnd = (ui, str, page, active, activechild) => str.AppendLine("</li>");


            Manager.Menu.Add(new Manager.MenuGroup()
            {
                InternalId = "SiteData",
                Name = "Site Data",
                CssClass = "content"
            });

            Manager.Menu.Single(m => m.InternalId == "SiteData").Items.Add(
                new Manager.MenuItem()
                {
                    Name = "Bookings",
                    Action = "Index",
                    Controller = "booking",
                    Permission = "",
                    SelectedActions = "index,edit,editbooking"
                });

            Manager.Menu.Single(m => m.InternalId == "SiteData").Items.Add(
                new Manager.MenuItem()
                {
                    Name = "Price Lists",
                    Action = "Index",
                    Controller = "pricelist",
                    Permission = "",
                    SelectedActions = "index,edit,editrow"
                });

            Manager.Menu.Single(m => m.InternalId == "SiteData").Items.Add(
                new Manager.MenuItem()
                {
                    Name = "Site Data",
                    Action = "Index",
                    Controller = "sitedata",
                    Permission = "",
                    SelectedActions = "index,edit"
                });

            Manager.Menu.Single(m => m.InternalId == "SiteData").Items.Add(
                new Manager.MenuItem()
                {
                    Name = "Links",
                    Action = "Index",
                    Controller = "socialmedia",
                    Permission = "",
                    SelectedActions = "index,edit"
                });

            Manager.Menu.Single(m => m.InternalId == "SiteData").Items.Add(
                new Manager.MenuItem()
                {
                    Name = "Guestbook",
                    Action = "Index",
                    Controller = "guestbook",
                    Permission = "",
                    SelectedActions = "index,edit"
                });
        }

        #region Helpers

        private static bool IsStartPage(Sitemap page)
        {
            return page.Controller == "StartPage";
        }

        private static string GenerateUrl(ISitemap page)
        {
            if (page != null)
            {
                if (!String.IsNullOrEmpty(page.Redirect))
                {
                    if (page.Redirect.Contains("://"))
                        return page.Redirect;
                    else if (page.Redirect.StartsWith("~/"))
                        return Url(page.Redirect);
                }
                if (page.IsStartpage)
                    return Url("~/");
                return Url("~/" + (!Config.PrefixlessPermalinks ?
                    Application.Current.Handlers.GetUrlPrefix("PERMALINK").ToLower() + "/" : "") + page.Permalink.ToLower());
            }
            return "";
        }

        private static string Url(string virtualpath)
        {
            var request = HttpContext.Current.Request;
            return virtualpath.Replace("~/", request.ApplicationPath + (request.ApplicationPath != "/" ? "/" : ""));
        }

        #endregion
    }
}