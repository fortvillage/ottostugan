﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Ottostugan.Areas.Manager.Models;
using Ottostugan.Areas.Manager.Services;
using Ottostugan.Models;
using Piranha.Areas.Manager.Controllers;
using Piranha.Entities;

namespace Ottostugan.Areas.Manager.Controllers
{
    public class BookingController : ManagerController
    {
        private readonly IManageBookingService _service;
        private readonly IPriceListService _priceListService;

        public BookingController() : base()
        {
            _service = new ManageBookingService();
            _priceListService = new PriceListService();
        }

        [HttpGet]
        public ActionResult Index()
        {
            
            Page[] apartments = _service.GetApartments();

            BookingListViewModel model = new BookingListViewModel();
            model.Apartments = _service.GetApartments();
            model.Houses = _service.GetHouses(); 

            var fromSession = Session["BookingFilter"];

            if (fromSession != null)
            {
                model.Filter = (BookingListFilter) fromSession;
            }
            else
            {
                model.Filter = new BookingListFilter {Status = null};

                int year = DateTime.Now.Year;
                int month = DateTime.Now.Month;

                var firstDay = new DateTime(year, month, 1);
                var lastDay = new DateTime(year, month, DateTime.DaysInMonth(year, month));

                model.Filter.FromDate = firstDay;
                model.Filter.ToDate = lastDay;
            }

            Booking[] bookings = _service.GetBookings(model.Filter);

            if (bookings != null && bookings.Length > 0)
            {
                
                var viewModels = bookings.Select(delegate(Booking booking)
                {
                    if (model.Filter.HouseId.HasValue)
                    {
                        apartments = apartments.Where(b => b.ParentId == model.Filter.HouseId.Value).ToArray();
                    }

                    var apartment = apartments.FirstOrDefault(a => a.Id == booking.ApartmentId);
                    if (apartment != null)
                    {
                        return new BookingViewModel
                        {
                            Booking = booking,
                            Apartment = apartment
                        };
                    }
                    return null;
                }).Where(vm => vm != null).OrderByDescending(vm => vm.Booking.FromDate).ToArray();
                model.Bookings = viewModels;
            }
            else
            {
                model.Bookings = new BookingViewModel[0];
            }

            return View(model);
        }

        public ActionResult Index(BookingListViewModel m)
        {
            var filter = m.Filter;
            Session["BookingFilter"] = filter;
            Page[] apartments = _service.GetApartments();

            if (filter.HouseId.HasValue)
            {
                apartments = apartments.Where(b => b.ParentId == filter.HouseId.Value).ToArray();
            }

            Booking[] bookings = _service.GetBookings(filter);

            var viewModels = bookings.Select(delegate(Booking booking)
            {
                var apartment = apartments.FirstOrDefault(a => a.Id == booking.ApartmentId);
                if (apartment != null)
                {
                    return new BookingViewModel
                    {
                        Booking = booking,
                        Apartment = apartments.First(a => a.Id == booking.ApartmentId)
                    };
                }
                return null;
            }).Where(apartment => apartment != null).OrderByDescending(vm => vm.Booking.FromDate).ToArray();

            return PartialView("Partial/BookingsList", viewModels);
        }

        [HttpGet]
        public ActionResult Edit()
        {
            var model = new ManageBookingsModel {Apartments = _service.GetApartments()};
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(ManageBookingsModel model)
        {
            if (model.SelectedApartments == null || model.SelectedApartments.Length == 0)
            {
                ModelState.AddModelError("SelectedApartments",
                    Resources.Extensions.BookingController_Edit_Please_select_at_least_one_apartment);
            }

            if (model.FromDate >= model.ToDate)
            {
                ModelState.AddModelError("FromDateShouldBeLessThanToDate", Resources.Extensions.BookingController_Edit__From_Date__should_be_less_than__To_Date_);
            }

            //if (model.FromDate.DayOfWeek != DayOfWeek.Sunday && model.FromDate.DayOfWeek != DayOfWeek.Thursday)
            //{
            //    ModelState.AddModelError("FromDateMustBeSundayOrThursday", Resources.Extensions.BookingController_Edit__From_Date__must_be_Sunday_or_Thursday);
            //}

            //if (model.ToDate.DayOfWeek != DayOfWeek.Sunday && model.ToDate.DayOfWeek != DayOfWeek.Thursday)
            //{
            //    ModelState.AddModelError("ToDateMustBeSundayOrThursday", Resources.Extensions.BookingController_Edit__To_Date__must_be_Sunday_or_Thursday);
            //}

            if (ModelState.IsValid)
            {
                if (model.SelectedApartments != null)
                {
                    var result = _service.SaveBookings(model.SelectedApartments.Select(apartment => new Booking
                    {
                        FromDate = model.FromDate,
                        ToDate = model.ToDate,
                        ApartmentId = apartment,
                        BookedByEmail = model.BookedByEmail,
                        BookedByName = model.BookedByName,
                        BookedByPhone = model.BookedByPhone,
                        Comments = model.Comments,
                        CreatedDate = DateTime.Now,
                        Status = model.Status.HasValue ? model.Status.Value : BookingStatus.Available,
                        IsPaidDeposit = model.IsPaidDeposit,
                        IsPaidFully = model.IsPaidFully,
                        Price = model.Price
                    }).ToArray());

                    if (!result)
                    {
                        ModelState.AddModelError("SaveBookingsError",
                            Resources.Extensions
                                .BookingController_Edit_Booked_apartments_exist_in_selection__Could_not_save_);
                    }
                    else
                    {
                        return RedirectToAction("Index");
                    }
                }
            }

            model.Apartments = _service.GetApartments();

            return View(model);
        }

        [HttpGet]
        public ActionResult Delete(int id = 0)
        {
            _service.Delete(id);
            return RedirectToAction("index");
        }

        [HttpGet]
        public ActionResult EditBooking(int id)
        {
            var booking = _service.GetBookingById(id);
            var model = new EditBookingModel { Booking = ToEditBookingViewModel(booking)};
            model.Apartment = _service.GetApartmentById(model.Booking.ApartmentId);
            return View(model);
        }

        [HttpPost]
        public ActionResult EditBooking(EditBookingModel model)
        {
            var booking = _service.GetBookingById(model.Booking.Id);

            if (model.Booking.FromDate >= model.Booking.ToDate)
            {
                ModelState.AddModelError("FromDateShouldBeLessThanToDate", Resources.Extensions.BookingController_Edit__From_Date__should_be_less_than__To_Date_);
            }

            //if (model.Booking.FromDate.DayOfWeek != DayOfWeek.Sunday && model.Booking.FromDate.DayOfWeek != DayOfWeek.Thursday)
            //{
            //    ModelState.AddModelError("FromDateMustBeSundayOrThursday", Resources.Extensions.BookingController_Edit__From_Date__must_be_Sunday_or_Thursday);
            //}

            //if (model.Booking.ToDate.DayOfWeek != DayOfWeek.Sunday && model.Booking.ToDate.DayOfWeek != DayOfWeek.Thursday)
            //{
            //    ModelState.AddModelError("ToDateMustBeSundayOrThursday", Resources.Extensions.BookingController_Edit__To_Date__must_be_Sunday_or_Thursday);
            //}

            if (ModelState.IsValid)
            {
                
                booking.FromDate = model.Booking.FromDate;
                booking.ToDate = model.Booking.ToDate;
                booking.Status = model.Booking.Status.HasValue ? model.Booking.Status.Value : BookingStatus.Available;
                booking.BookedByEmail = model.Booking.BookedByEmail;
                booking.BookedByName = model.Booking.BookedByName;
                booking.BookedByPhone = model.Booking.BookedByPhone;
                booking.Comments = model.Booking.Comments;
                booking.IsPaidDeposit = model.Booking.IsPaidDeposit;
                booking.IsPaidFully = model.Booking.IsPaidFully;
                booking.Price = model.Booking.Price;
                var result = _service.SaveBooking(booking);
                if (!result)
                {
                    ModelState.AddModelError("SaveBookingsError",
                        Resources.Extensions
                            .BookingController_Edit_Booked_apartments_exist_in_selection__Could_not_save_);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }

            model.Booking.ApartmentId = booking.ApartmentId;

            model.Apartment = _service.GetApartmentById(model.Booking.ApartmentId);

            return View(model);
        }

        public static IEnumerable<Tuple<DateTime, DateTime>> SplitDateRange(DateTime start, DateTime end)
        {
            DateTime chunkEnd = start.AddDays(1);
            while (chunkEnd < end)
            {
                if (chunkEnd.DayOfWeek == DayOfWeek.Thursday || chunkEnd.DayOfWeek == DayOfWeek.Sunday)
                {
                    yield return Tuple.Create(start, chunkEnd);
                    start = chunkEnd;
                }
                chunkEnd = chunkEnd.AddDays(1);
            }
            yield return Tuple.Create(start, end);
        }

        private EditBookingViewModel ToEditBookingViewModel(Booking booking)
        {
            return new EditBookingViewModel()
            {
                Id = booking.Id,
                IsPaidDeposit = booking.IsPaidDeposit,
                IsPaidFully = booking.IsPaidFully,
                ApartmentId = booking.ApartmentId,
                BookedByEmail = booking.BookedByEmail,
                BookedByName = booking.BookedByName,
                BookedByPhone = booking.BookedByPhone,
                Comments = booking.Comments,
                CreatedDate = booking.CreatedDate,
                FromDate = booking.FromDate,
                Status = booking.Status,
                ToDate = booking.ToDate,
                Price = booking.Price

            };
        }
    }
}