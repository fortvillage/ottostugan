﻿using Ottostugan.Areas.Manager.Services;
using Piranha.Areas.Manager.Controllers;
using System.Web.Mvc;
using Ottostugan.Models;
using GuestFeedbackViewModel = Ottostugan.Areas.Manager.Models.GuestFeedbackViewModel;

namespace Ottostugan.Areas.Manager.Controllers
{
    public class GuestBookController : ManagerController
    {
        private readonly IGuestBookService _service;

        public GuestBookController() : base()
        {
            _service = new GuestBookService();
        }

        public ActionResult Index()
        {
            GuestFeedback[] feedbacks = _service.GetFeedbacks();

            return View(feedbacks);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            GuestFeedback feedback = _service.GetFeedbackById(id);
            GuestFeedbackViewModel model = new GuestFeedbackViewModel()
            {
                Comment = feedback.Comment,
                CreatedDate = feedback.CreatedDate,
                From = feedback.From,
                Id = feedback.Id,
                Published = feedback.Published
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(GuestFeedbackViewModel model)
        {
            if (ModelState.IsValid)
            {
                var feedback = _service.GetFeedbackById(model.Id);

                feedback.Comment = model.Comment;
                feedback.From = model.From;
                feedback.Published = model.Published;

                _service.SaveFeedback(feedback);

                return RedirectToAction("Index");
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Delete(int id = 0)
        {
            _service.DeleteFeedback(id);
            return RedirectToAction("Index");
        }
    }
}