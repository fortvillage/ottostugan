﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Ottostugan.Areas.Manager.Models;
using Ottostugan.Areas.Manager.Services;
using Ottostugan.Models;
using Piranha.Areas.Manager.Controllers;
using Piranha.WebPages;

namespace Ottostugan.Areas.Manager.Controllers
{
    public class PriceListController : ManagerController
    {
        private readonly IPriceListService _service;

        public PriceListController() : base()
        {
            _service = new PriceListService();
        }

        [HttpGet]
        public ActionResult Index(int st = 0)
        {
            var priceLists = _service.GetPriceLists();
            var cantDeletePriceList = st == 1;
            return View(new Tuple<bool, PriceList[]>(cantDeletePriceList, priceLists));
        }

        [HttpGet]
        public ActionResult EditRow(int? rowId, int priceListId)
        {
            var row = rowId.HasValue ? _service.GetPriceListRowById(rowId.Value) : null;

            var model = new PriceListRowModel();

            if (row == null)
            {
                model.PriceListId = priceListId;
            }
            else
            {
                model.FromDate = row.FromDate;
                model.Id = row.Id;
                model.PriceFullWeek = row.PriceFullWeek;
                model.PriceListId = row.PriceListId;
                model.PriceSplittedSunThurs = row.PriceSplittedSunThurs;
                model.PriceSplittedThursSun = row.PriceSplittedThursSun;
                model.ToDate = row.ToDate;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult EditRow(PriceListRowModel model)
        {
            if (model.FromDate >= model.ToDate)
            {
                ModelState.AddModelError("FromDateShouldBeLessThanToDate", Resources.Extensions.BookingController_Edit__From_Date__should_be_less_than__To_Date_);
            }

            //if (model.FromDate.DayOfWeek != DayOfWeek.Sunday)
            //{
            //    ModelState.AddModelError("FromDateMustBeSunday", Resources.Extensions.BookingController_Edit__From_Date__must_be_Sunday_or_Thursday);
            //}

            //if (model.ToDate.DayOfWeek != DayOfWeek.Sunday)
            //{
            //    ModelState.AddModelError("ToDateMustBeSunday", Resources.Extensions.BookingController_Edit__To_Date__must_be_Sunday_or_Thursday);
            //}

            if (ModelState.IsValid)
            {
                var priceListRow = new PriceListRow()
                {
                    Id = model.Id,
                    FromDate = model.FromDate,
                    ToDate = model.ToDate,
                    PriceFullWeek = model.PriceFullWeek,
                    PriceSplittedSunThurs = model.PriceSplittedSunThurs,
                    PriceSplittedThursSun = model.PriceSplittedThursSun,
                    PriceListId = model.PriceListId
                };


                var result = _service.SavePriceListRow(priceListRow);

                if (!result)
                {
                    ModelState.AddModelError("SavePriceListRow", Resources.Extensions.PriceListController_EditRow_An_error_occured_while_saving_price_list_row);
                }
                else
                {
                    return RedirectToAction("edit", new {id = model.PriceListId});
                }
            }

            return View(model);
        }

        [HttpGet]
        public ActionResult DeleteRow(int id = 0)
        {
            var row = _service.GetPriceListRowById(id);
            if (row != null)
            {
                var result = _service.DeleteRow(row.Id);
                return RedirectToAction("edit", new {id = row.PriceListId});
            }
            return RedirectToAction("index");
        }

        [HttpGet]
        public ActionResult Delete(int id = 0)
        {
            var priceList = _service.GetPriceListById(id);
            if (priceList != null)
            {
                var result = _service.Delete(priceList.Id);
                if (result)
                {
                    return RedirectToAction("index");
                }
                else
                {
                    return RedirectToAction("index", new {st = 1});
                }
            }
            return RedirectToAction("index");
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            var model = new PriceListModel();

            if (id.HasValue)
            {
                var priceList = _service.GetPriceListById(id.Value);
                model.Id = priceList.Id;
                model.Name = priceList.Name;

                model.PriceListRows =
                    priceList.PriceListRows.Select(
                        x =>
                            new PriceListRowModel()
                            {
                                Id = x.Id,
                                PriceListId = x.PriceListId,
                                FromDate = x.FromDate,
                                ToDate = x.ToDate,
                                PriceFullWeek = x.PriceFullWeek,
                                PriceSplittedSunThurs = x.PriceSplittedSunThurs,
                                PriceSplittedThursSun = x.PriceSplittedThursSun
                            }).ToList();

            }
            else
            {
                model.PriceListRows = new List<PriceListRowModel>();
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(PriceListModel m)
        {

            if (ModelState.IsValid)
            {

                var priceList = new PriceList()
                {
                    Id = m.Id,
                    Name = m.Name
                };

                var result = _service.SavePriceList(priceList);

                if (result)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("SavePriceListError", Resources.Extensions.PriceListController_Edit_Unable_to_save_price_list);
                }
            }

            return View(m);
        }

    }
}
