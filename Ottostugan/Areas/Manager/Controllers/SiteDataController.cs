﻿using System.Data.Entity.Migrations;
using System.Linq;
using System.Web.Mvc;
using Ottostugan.DAL;
using Ottostugan.Models;
using Piranha.Areas.Manager.Controllers;

namespace Ottostugan.Areas.Manager.Controllers
{
    public class SiteDataController : ManagerController
    {
        /*[Access(Function = "ADMIN_PRODUCT")]*/
        public ActionResult Index()
        {
            Preference[] preferences;

            using (var db = new OttostuganContext())
            {
                preferences = db.Preferences.ToArray();
            }

            return View(preferences);
        }

        /*[Access(Function = "ADMIN_PRODUCT")]*/
        [HttpGet]
        public ActionResult Edit(int id = 0)
        {

            Preference preference;

            using (var db = new OttostuganContext())
            {
                preference = db.Preferences.FirstOrDefault(p => p.Id == id);
            }

            if (preference == null)
            {
                preference = new Preference() { Id = 0 };
            }

            return View(preference);
        }

        [HttpPost]
        public ActionResult Edit(Preference model)
        {
            using (var db = new OttostuganContext())
            {
                db.Preferences.AddOrUpdate(model);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Delete(int id = 0)
        {
            using (var db = new OttostuganContext())
            {
                var preference = db.Preferences.FirstOrDefault(p => p.Id == id);
                if (preference != null)
                {
                    db.Preferences.Remove(preference);
                    db.SaveChanges();
                }
            }
            return RedirectToAction("Index");
        }
    }
}
