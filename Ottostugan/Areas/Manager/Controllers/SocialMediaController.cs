﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ottostugan.DAL;
using Ottostugan.Models;
using Piranha.Areas.Manager.Controllers;

namespace Ottostugan.Areas.Manager.Controllers
{
    public class SocialMediaController : ManagerController
    {
        //
        // GET: /Manager/SocialMedia/

        public ActionResult Index()
        {
            SocialMediaLink[] socialMediaLinks;

            using (var db = new OttostuganContext())
            {
                socialMediaLinks = db.SocialMediaLinks.ToArray();
            }

            return View(socialMediaLinks);
        }

        [HttpGet]
        public ActionResult Edit(int id = 0)
        {

            SocialMediaLink socialMediaLink;

            using (var db = new OttostuganContext())
            {
                socialMediaLink = db.SocialMediaLinks.FirstOrDefault(p => p.Id == id);
            }

            if (socialMediaLink == null)
            {
                socialMediaLink = new SocialMediaLink() { Id = 0 };
            }

            return View(socialMediaLink);
        }

        [HttpPost]
        public ActionResult Edit(SocialMediaLink model)
        {
            using (var db = new OttostuganContext())
            {
                db.SocialMediaLinks.AddOrUpdate(model);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Delete(int id = 0)
        {
            using (var db = new OttostuganContext())
            {
                var socialMediaLink = db.SocialMediaLinks.FirstOrDefault(p => p.Id == id);
                if (socialMediaLink != null)
                {
                    db.SocialMediaLinks.Remove(socialMediaLink);
                    db.SaveChanges();
                }
            }
            return RedirectToAction("Index");
        }
    }
}
