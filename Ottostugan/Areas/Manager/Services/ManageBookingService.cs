﻿using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using Ottostugan.Areas.Manager.Models;
using Ottostugan.DAL;
using Ottostugan.Extensions.PageTypes;
using Ottostugan.Models;
using Ottostugan.Services;
using Piranha;
using Piranha.Entities;

namespace Ottostugan.Areas.Manager.Services
{
    public class ManageBookingService : IManageBookingService
    {
        private readonly IApartmentsCachedService _apartmentsCachedService;
        private readonly IHousePageCachedService _housesCachedService;

        public ManageBookingService()
        {
             _apartmentsCachedService = new ApartmentsCachedService();
            _housesCachedService = new HousePageCachedService();
        }

        public Page[] GetApartments()
        {
            return _apartmentsCachedService.GetApartmetPages()
                .OrderBy(p => p.Title)
                .ThenBy(p => p.Seqno)
                .ToArray();
        }

        public Page[] GetHouses()
        {
            return _housesCachedService.GetHousePages()
                .OrderBy(p => p.Title)
                .ThenBy(p => p.Seqno)
                .ToArray();
        }

        public bool SaveBookings(Booking[] bookings)
        {
            using (var db = new OttostuganContext())
            {
                foreach (var booking in bookings)
                {
                    var bookingFromDb = db.Bookings.FirstOrDefault(b => b.ApartmentId == booking.ApartmentId && (b.FromDate < booking.ToDate && booking.FromDate < b.ToDate));

                    if (bookingFromDb != null)
                    {
                        return false;
                    }

                    db.Bookings.AddOrUpdate(booking);
                }

                db.SaveChanges();
            }

            return true;
        }

        public bool SaveBooking(Booking booking)
        {
            using (var db = new OttostuganContext())
            {
                var bookingFromDb = db.Bookings.FirstOrDefault(
                    b => b.ApartmentId == booking.ApartmentId
                        && (b.FromDate < booking.ToDate && booking.FromDate < b.ToDate) && b.Id != booking.Id);
                
                if (bookingFromDb != null)
                    return false;

                db.Bookings.AddOrUpdate(booking);
                db.SaveChanges();
            }

            return true;
        }

        public Booking GetBookingById(int id)
        {
            using (var db = new OttostuganContext())
            {
                return db.Bookings.FirstOrDefault(x => x.Id == id);
            }
        }

        public Page GetApartmentById(Guid id)
        {
            return _apartmentsCachedService.GetApartmentById(id);
        }

        public Booking[] GetBookings()
        {
            using (var db = new OttostuganContext())
            {
                return db.Bookings.ToArray();
            }
        }

        public Booking[] GetBookings(BookingListFilter filter)
        {
            using (var db = new OttostuganContext())
            {
                var bookings = db.Bookings.AsQueryable();

                if (filter.ApartmentId.HasValue)
                {
                    bookings = bookings.Where(b => b.ApartmentId == filter.ApartmentId.Value);
                }

                bookings = bookings.Where(b => b.FromDate >= filter.FromDate);

                bookings = bookings.Where(b => b.ToDate <= filter.ToDate);

                if (filter.Status.HasValue)
                {
                    bookings = bookings.Where(b => b.Status == filter.Status.Value);
                }

                return bookings.ToArray();
            }
        }

        public void Delete(int bookingId)
        {
            using (var db = new OttostuganContext())
            {
                var booking = db.Bookings.FirstOrDefault(b => b.Id == bookingId);
                if (booking == null) return;
                db.Bookings.Remove(booking);
                db.SaveChanges();
            }
        }
    }
}