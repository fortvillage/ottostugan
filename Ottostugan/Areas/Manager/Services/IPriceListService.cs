﻿using Ottostugan.Entities;
using Ottostugan.Models;

namespace Ottostugan.Areas.Manager.Services
{
    public interface IPriceListService
    {
        PriceList[] GetPriceLists();
        PriceList GetPriceListById(int id);
        PriceListRow GetPriceListRowById(int id);
        bool SavePriceList(PriceList priceList);
        bool SavePriceListRow(PriceListRow priceListRow);
        bool DeleteRow(int id);
        bool Delete(int priceListId);
        bool TryGetBookingPrice(BookingEntity booking, out decimal price);
        bool TryGetBookingPrice(Booking booking, out decimal price);
    }
}