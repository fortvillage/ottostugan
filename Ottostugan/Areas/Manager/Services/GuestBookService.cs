using System.Data.Entity.Migrations;
using System.Linq;
using Ottostugan.DAL;
using Ottostugan.Models;

namespace Ottostugan.Areas.Manager.Services
{
    public class GuestBookService : IGuestBookService
    {
        public GuestFeedback[] GetFeedbacks()
        {
            using (var db = new OttostuganContext())
            {
                return db.GuestFeedbacks.ToArray();
            }
        }

        public GuestFeedback GetFeedbackById(int id)
        {
            using (var db = new OttostuganContext())
            {
                return db.GuestFeedbacks.FirstOrDefault(f => f.Id == id);
            }
        }

        public bool SaveFeedback(GuestFeedback feedback)
        {
            using (var db = new OttostuganContext())
            {
                db.GuestFeedbacks.AddOrUpdate(feedback);
                db.SaveChanges();
            }
            return true;
        }

        public bool DeleteFeedback(int id)
        {
            using (var db = new OttostuganContext())
            {
                var feedback = db.GuestFeedbacks.FirstOrDefault(p => p.Id == id);
                if (feedback != null)
                {
                    db.GuestFeedbacks.Remove(feedback);
                    db.SaveChanges();
                }
            }
            return true;
        }
    }
}