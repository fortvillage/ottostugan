﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using System.Data.Entity.Migrations;
using System.Linq;
using Ottostugan.DAL;
using Ottostugan.Entities;
using Ottostugan.Extensions.PageTypes;
using Ottostugan.Extensions.Regions;
using Ottostugan.Models;
using Ottostugan.Services;
using Piranha;
using Piranha.Models;
using Piranha.WebPages;

namespace Ottostugan.Areas.Manager.Services
{
    public class PriceListService : IPriceListService
    {
        private readonly IApartmentsCachedService _apartmentsCachedService;

        public PriceListService()
        {
            _apartmentsCachedService = new ApartmentsCachedService();
        }

        public PriceList[] GetPriceLists()
        {
            using (var db = new OttostuganContext())
            {
                return db.PriceLists.Include("PriceListRows").ToArray();
            }
        }

        public PriceList GetPriceListById(int id)
        {
            using (var db = new OttostuganContext())
            {
                return db.PriceLists.Include("PriceListRows").FirstOrDefault(pl => pl.Id == id);
            }
        }

        public PriceListRow GetPriceListRowById(int id)
        {
            using (var db = new OttostuganContext())
            {
                return db.PriceListRows.FirstOrDefault(pl => pl.Id == id);
            }
        }

        public bool SavePriceList(PriceList priceList)
        {
            using (var db = new OttostuganContext())
            {
                db.PriceLists.AddOrUpdate(priceList);
                db.SaveChanges();
            }
            return true;
        }

        public bool SavePriceListRow(PriceListRow priceListRow)
        {
            using (var db = new OttostuganContext())
            {
                var priceList = GetPriceListById(priceListRow.PriceListId);
                if (priceList == null)
                    return false;
                db.PriceListRows.AddOrUpdate(priceListRow);
                db.SaveChanges();
            }
            return true;
        }

        public bool DeleteRow(int id)
        {
            using (var db = new OttostuganContext())
            {
                var row = db.PriceListRows.FirstOrDefault(p => p.Id == id);
                if (row != null)
                {
                    db.PriceListRows.Remove(row);
                    db.SaveChanges();
                }
            }

            return true;
        }

        public bool Delete(int priceListId)
        {
            using (var db = new OttostuganContext())
            {
                var priceList = db.PriceLists.Include("PriceListRows").FirstOrDefault(p => p.Id == priceListId);
                if (priceList != null)
                {
                    using (var pdb = new DataContext())
                    {
                        var apartments = pdb.Pages
                            .Include("Template")
                            .Include("Regions")
                            .Where(p => p.Template.Type == (typeof(ApartmentPage)).FullName)
                            .ToList();



                        foreach (var apartment in apartments)
                        {
                            var region = apartment.Regions.FirstOrDefault(x => x.Body.GetType() == typeof(PriceListRegion));
                            if (region != null)
                            {
                                var body = region.GetBody<PriceListRegion>();
                                if (body != null && body.Id.HasValue)
                                {
                                    if (priceList.Id == body.Id.Value)
                                    {
                                        return false;
                                    }
                                }
                            }
                        }
                    }

                    db.PriceListRows.RemoveRange(priceList.PriceListRows);
                    db.PriceLists.Remove(priceList);
                    db.SaveChanges();
                }
            }

            return true;
        }

        public bool TryGetBookingPrice(BookingEntity booking, out decimal price)
        {
            return TryGetBookingPrice(booking.ApartmentId, booking.FromDate, booking.ToDate, out price);
        }

        public bool TryGetBookingPrice(Booking booking, out decimal price)
        {
            return TryGetBookingPrice(booking.ApartmentId, booking.FromDate, booking.ToDate, out price);
        }

        private bool TryGetBookingPrice(Guid apartmentId, DateTime fromDate, DateTime toDate, out decimal price)
        {
            price = 0;

            var apartmentPage = _apartmentsCachedService.GetApartmentById(apartmentId);

            if (apartmentPage == null)
                return false;

            var am = _apartmentsCachedService.GetApartmentPageModel(apartmentPage.Permalink.Name);
            PriceList priceList = am.PriceList;

            if (priceList == null)
                return false;

            var startDate = fromDate;
            var endDate = toDate;


            var priceListRow =
                priceList.PriceListRows.FirstOrDefault(row => row.FromDate <= startDate && row.ToDate >= endDate);

            if (priceListRow == null)
                return false;

            if (startDate.DayOfWeek == DayOfWeek.Sunday && endDate.DayOfWeek == DayOfWeek.Sunday)
            {
                price = (decimal)priceListRow.PriceFullWeek;
            }
            else if (startDate.DayOfWeek == DayOfWeek.Thursday && endDate.DayOfWeek == DayOfWeek.Sunday)
            {
                price = (decimal)priceListRow.PriceSplittedThursSun;
            }
            else if (startDate.DayOfWeek == DayOfWeek.Sunday && endDate.DayOfWeek == DayOfWeek.Thursday)
            {
                price = (decimal)priceListRow.PriceSplittedSunThurs;
            }
            else
            {
                int days = (int)(endDate - startDate).TotalDays;
                var pricePerDay = (decimal)priceListRow.PriceFullWeek / 5;
                price = days * pricePerDay;
                //return false;
            }

            if (price == 0)
                return false;

            return true;
        }
    }
}