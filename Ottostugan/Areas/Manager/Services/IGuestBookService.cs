using Ottostugan.Models;

namespace Ottostugan.Areas.Manager.Services
{
    public interface IGuestBookService
    {
        GuestFeedback[] GetFeedbacks();
        GuestFeedback GetFeedbackById(int id);
        bool SaveFeedback(GuestFeedback feedback);
        bool DeleteFeedback(int id);
    }
}