﻿using System;
using Ottostugan.Areas.Manager.Models;
using Ottostugan.Models;
using Piranha.Entities;

namespace Ottostugan.Areas.Manager.Services
{
    public interface IManageBookingService
    {
        Page[] GetApartments();
        Page[] GetHouses();
        bool SaveBookings(Booking[] bookings);
        bool SaveBooking(Booking booking);
        Booking GetBookingById(int id);
        Page GetApartmentById(Guid id);
        Booking[] GetBookings();
        Booking[] GetBookings(BookingListFilter filter);
        void Delete(int bookingId);
    }
}