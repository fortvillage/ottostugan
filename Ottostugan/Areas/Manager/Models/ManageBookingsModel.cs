﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Ottostugan.Models;
using Piranha.Entities;

namespace Ottostugan.Areas.Manager.Models
{
    public class ManageBookingsModel
    {
        public SelectList BookingStatusesSelectList { get; set; }
        public Page[] Apartments { get; set; }

        public ManageBookingsModel()
        {
            var statuses = from BookingStatus b in Enum.GetValues(typeof(BookingStatus))
                             where b != BookingStatus.Available
                             select new { ID = (byte)b, Name = b.ToString() };
            BookingStatusesSelectList = new SelectList(statuses, "ID", "Name");
        }

        [Display(ResourceType = typeof (Resources.Extensions), Name = "ManageBookingsModel_SelectedApartments_Apartments")]
        public Guid[] SelectedApartments { get; set; }

        [Display(ResourceType = typeof (Resources.Extensions), Name = "ManageBookingsModel_FromDate_From_Date")]
        [Required(ErrorMessageResourceType = typeof (Resources.Extensions), ErrorMessageResourceName = "ManageBookingsModel_FromDate__From_Date__field_is_required")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime FromDate { get; set; }

        [Display(ResourceType = typeof (Resources.Extensions), Name = "ManageBookingsModel_ToDate_To_Date")]
        [Required(ErrorMessageResourceType = typeof (Resources.Extensions), ErrorMessageResourceName = "ManageBookingsModel_ToDate__To_Date__field_is_required")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ToDate { get; set; }

        [Display(ResourceType = typeof (Resources.Extensions), Name = "ManageBookingsModel_Status_Status")]
        [Required]
        public BookingStatus? Status { get; set; }

        [Display(ResourceType = typeof (Resources.Extensions), Name = "ManageBookingsModel_BookedByName_Booked_By_Name")]
        public string BookedByName { get; set; }

        [Display(ResourceType = typeof (Resources.Extensions), Name = "ManageBookingsModel_BookedByEmail_Booked_By_Email")]
        public string BookedByEmail { get; set; }

        [Display(ResourceType = typeof (Resources.Extensions), Name = "ManageBookingsModel_BookedByPhone_Booked_By_Phone")]
        public string BookedByPhone { get; set; }

        [Display(ResourceType = typeof (Resources.Extensions), Name = "ManageBookingsModel_Comments_Comments")]
        public string Comments { get; set; }

        [Display(ResourceType = typeof (Resources.Extensions), Name = "ManageBookingsModel_IsPaidDeposit_Paid_Deposit")]
        public bool IsPaidDeposit { get; set; }

        [Display(ResourceType = typeof (Resources.Extensions), Name = "ManageBookingsModel_IsPaidFully_Paid_Fully")]
        public bool IsPaidFully { get; set; }

        public DateTime CreatedDate { get; set; }

        [Display(ResourceType = typeof(Resources.Extensions), Name = "ManageBookingsModel_Price_Price")]
        public Decimal Price { get; set; }
    }
}