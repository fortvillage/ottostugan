﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ottostugan.Areas.Manager.Models
{
    public class PriceListModel
    {
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof (Resources.Extensions), ErrorMessageResourceName = "Preference_Name__Name__field_is_required")]
        public string Name { get; set; }

        public List<PriceListRowModel> PriceListRows { get; set; } 
    }
}