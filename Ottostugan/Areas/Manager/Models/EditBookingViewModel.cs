﻿using System;
using System.ComponentModel.DataAnnotations;
using Ottostugan.Models;

namespace Ottostugan.Areas.Manager.Models
{
    public class EditBookingViewModel
    {
        public int Id { get; set; }

        public Guid ApartmentId { get; set; }

        [Display(ResourceType = typeof(Resources.Extensions), Name = "ManageBookingsModel_FromDate_From_Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime FromDate { get; set; }

        [Display(ResourceType = typeof(Resources.Extensions), Name = "ManageBookingsModel_ToDate_To_Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ToDate { get; set; }

        [Display(ResourceType = typeof(Resources.Extensions), Name = "ManageBookingsModel_Status_Status")]
        [Required]
        public BookingStatus? Status { get; set; }

        [Display(ResourceType = typeof(Resources.Extensions), Name = "ManageBookingsModel_BookedByName_Booked_By_Name")]
        public string BookedByName { get; set; }

        [Display(ResourceType = typeof(Resources.Extensions), Name = "ManageBookingsModel_BookedByEmail_Booked_By_Email")]
        public string BookedByEmail { get; set; }

        [Display(ResourceType = typeof(Resources.Extensions), Name = "ManageBookingsModel_BookedByPhone_Booked_By_Phone")]
        public string BookedByPhone { get; set; }

        [Display(ResourceType = typeof(Resources.Extensions), Name = "ManageBookingsModel_Comments_Comments")]
        public string Comments { get; set; }

        public DateTime CreatedDate { get; set; }

        [Display(ResourceType = typeof(Resources.Extensions), Name = "ManageBookingsModel_IsPaidDeposit_Paid_Deposit")]
        public bool IsPaidDeposit { get; set; }

        [Display(ResourceType = typeof(Resources.Extensions), Name = "ManageBookingsModel_IsPaidFully_Paid_Fully")]
        public bool IsPaidFully { get; set; }

        [Display(ResourceType = typeof(Resources.Extensions), Name = "ManageBookingsModel_Price_Price")]
        public Decimal Price { get; set; }
    }
}