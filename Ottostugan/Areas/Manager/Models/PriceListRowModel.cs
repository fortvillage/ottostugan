﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Ottostugan.Areas.Manager.Models
{
    public class PriceListRowModel
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public int PriceListId { get; set; }

        [Required(ErrorMessageResourceType = typeof (Resources.Extensions), ErrorMessageResourceName = "PriceListRowModel_PriceFullWeek_This_field_is_required")]
        [Display(ResourceType = typeof (Resources.Extensions), Name = "PriceListRowModel_PriceFullWeek_Price_Full_Week")]
        public double PriceFullWeek { get; set; }

        [Required (ErrorMessageResourceType = typeof (Resources.Extensions), ErrorMessageResourceName = "PriceListRowModel_PriceFullWeek_This_field_is_required")]
        [Display(ResourceType = typeof (Resources.Extensions), Name = "PriceListRowModel_PriceSplittedSunThurs_Price_Sun_Thurs")]
        public double PriceSplittedSunThurs { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Extensions), ErrorMessageResourceName = "PriceListRowModel_PriceFullWeek_This_field_is_required")]
        [Display(ResourceType = typeof (Resources.Extensions), Name = "PriceListRowModel_PriceSplittedThursSun_Price_Thurs_Sun")]
        public double PriceSplittedThursSun { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Extensions), ErrorMessageResourceName = "PriceListRowModel_PriceFullWeek_This_field_is_required")]
        [Display(ResourceType = typeof (Resources.Extensions), Name = "ManageBookingsModel_FromDate_From_Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime FromDate { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Extensions), ErrorMessageResourceName = "PriceListRowModel_PriceFullWeek_This_field_is_required")]
        [Display(ResourceType = typeof (Resources.Extensions), Name = "ManageBookingsModel_ToDate_To_Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ToDate { get; set; }
    }
}