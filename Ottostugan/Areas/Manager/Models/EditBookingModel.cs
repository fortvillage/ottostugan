﻿using System;
using System.Linq;
using System.Web.Mvc;
using Ottostugan.Models;
using Piranha.Entities;

namespace Ottostugan.Areas.Manager.Models
{
    public class EditBookingModel
    {
        public SelectList BookingStatusesSelectList { get; set; }
        public EditBookingModel()
        {
            var statuses = from BookingStatus b in Enum.GetValues(typeof(BookingStatus))
                             where b != BookingStatus.Available
                             select new { ID = (byte)b, Name = b.ToString() };
            BookingStatusesSelectList = new SelectList(statuses, "ID", "Name");
        }
        public EditBookingViewModel Booking { get; set; }
        public Page Apartment { get; set; }
    }
}