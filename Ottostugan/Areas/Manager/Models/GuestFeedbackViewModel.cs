﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Ottostugan.Areas.Manager.Models
{
    public class GuestFeedbackViewModel
    {
        public int Id { get; set; }
        public bool Published { get; set; }
        [Required]
        public string From { get; set; }
        [Required]
        public string Comment { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}