﻿using System;
using System.ComponentModel.DataAnnotations;
using Ottostugan.Models;

namespace Ottostugan.Areas.Manager.Models
{
    public class BookingListFilter
    {
        public Guid? HouseId { get; set; }
        public Guid? ApartmentId { get; set; }
        [DisplayFormat(DataFormatString = @"{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime FromDate { get; set; }
        [DisplayFormat(DataFormatString = @"{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ToDate { get; set; }
        public BookingStatus? Status { get; set; }
    }
}