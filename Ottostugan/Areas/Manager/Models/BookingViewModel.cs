﻿using Ottostugan.Models;
using Piranha.Entities;

namespace Ottostugan.Areas.Manager.Models
{
    public class BookingViewModel
    {
        public Booking Booking { get; set; }
        public Page Apartment { get; set; }
    }
}