﻿using System;
using System.Linq;
using System.Web.Mvc;
using Ottostugan.Models;
using Piranha.Entities;

namespace Ottostugan.Areas.Manager.Models
{
    public class BookingListViewModel
    {
        public SelectList BookingStatusesSelectList { get; set; }
        public Page[] Apartments { get; set; }
        public Page[] Houses { get; set; }

        public BookingListViewModel()
        {
            var statuses = (from BookingStatus b in Enum.GetValues(typeof(BookingStatus))
                             select new { ID = (byte?)b, Name = b.ToString() }).ToList();

            statuses.Insert(0, new { ID = (byte?)null, Name = "All"});

            BookingStatusesSelectList = new SelectList(statuses, "ID", "Name");
        }

        public BookingListFilter Filter { get; set; }

        public BookingViewModel[] Bookings { get; set; }
    }
}