﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Ottostugan.Models;
using Piranha.Data;

namespace Ottostugan.DAL
{
    public class OttostuganContext : DbContext
    {
        public OttostuganContext()
            : base("name=piranha")
        {
        }

        public DbSet<Preference> Preferences { get; set; }

        public DbSet<Booking> Bookings { get; set; }

        public DbSet<SocialMediaLink> SocialMediaLinks { get; set; }

        public DbSet<PriceList> PriceLists { get; set; }

        public DbSet<PriceListRow> PriceListRows { get; set; }

        public DbSet<GuestFeedback> GuestFeedbacks { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}