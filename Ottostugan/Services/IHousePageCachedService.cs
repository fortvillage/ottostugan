﻿using System;
using Ottostugan.Extensions.PageModels;
using Piranha.Entities;

namespace Ottostugan.Services
{
    public interface IHousePageCachedService
    {
        Page[] GetHousePages();
        Page GetHousePageById(Guid id);
    }
}