﻿using System;
using Ottostugan.Entities;
using Ottostugan.Models;
using Ottostugan.ViewModels;
using Piranha.Entities;

namespace Ottostugan.Services
{
    public interface IBookingService
    {
        BookingCalendarEvent[] GetBookingsByApartmentId(Guid id, SearchType? searchType);
        BookingCalendarEvent[] GetBookingsByApartmentId(Guid id, SearchType? searchType, DateTime startDate, DateTime endDate, DateTime fromDateTime, DateTime toDateTime);
        BookingCalendarEvent[] SearchBookings(int? adults, int? children, SearchType? searchType, DateTime startDate, DateTime endDate, DateTime fromDateTime, DateTime toDateTime);
        ApartmentBooking[] SearchApartments(int? adults, int? children, DateTime startDate, DateTime endDate, BookingStatus? status, SearchType? searchType);
        BookingEntity GetBookingById(int bookingId);
    }
}