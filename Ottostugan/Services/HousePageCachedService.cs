﻿using System;
using System.Data.Entity;
using System.Linq;
using Ottostugan.Caching;
using Ottostugan.Extensions.PageTypes;
using Piranha.Entities;

namespace Ottostugan.Services
{
    public class HousePageCachedService : IHousePageCachedService
    {
        private readonly ICacheService _cacheService;

        private const string HousePagesCacheId = "AllHousePages";

        public HousePageCachedService()
        {
            _cacheService = new InMemoryCache();
        }

        public Page[] GetHousePages()
        {
            return _cacheService.Get(HousePagesCacheId, FetchHousePagesFromDb);
        }

        public Page GetHousePageById(Guid id)
        {
            return _cacheService.Get(HousePagesCacheId, FetchHousePagesFromDb).FirstOrDefault(p=>p.Id == id);
        }

        private Page[] FetchHousePagesFromDb()
        {
            using (var db = new Piranha.DataContext())
            {
                var query = db.Pages
                    .Include("Permalink")
                    .Include("Template")
                    .Include("Properties")
                    .Include("Regions")
                    .Where(p => p.Template.Type == (typeof(HousePage)).FullName);

                return query.ToArray();
            }
        }
    }
}