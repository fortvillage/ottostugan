﻿using Ottostugan.Helpers;
using Ottostugan.Resources;

namespace Ottostugan.Services
{
    public interface ISendEmailService
    {
        void SendEmail(string subject, string body);
    }
}