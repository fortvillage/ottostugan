﻿using System;
using System.Collections.Generic;
using System.Web;
using Ottostugan.Extensions.PageModels;
using Piranha.Entities;

namespace Ottostugan.Services
{
    public interface IApartmentsCachedService
    {
        Page[] GetApartmetPages();
        Page GetApartmentById(Guid id);
        ApartmentPageModel GetApartmentPageModel(string permalink);
    }
}