﻿using Ottostugan.Models;

namespace Ottostugan.Services
{
    public interface IGuestBookService
    {
        GuestFeedback[] GetPublishedFeedbacks();
        bool SaveFeedback(GuestFeedbackViewModel model);
    }
}