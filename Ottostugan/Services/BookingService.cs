﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ottostugan.Areas.Manager.Services;
using Ottostugan.DAL;
using Ottostugan.Entities;
using Ottostugan.Extensions.PageModels;
using Ottostugan.Helpers;
using Ottostugan.Models;
using Ottostugan.ViewModels;
using Piranha.Entities;

namespace Ottostugan.Services
{
    public class BookingService : IBookingService
    {
        private readonly IApartmentsCachedService _apartmentsCachedService;
        private readonly IPriceListService _priceListService;

        public BookingService()
        {
            _apartmentsCachedService = new ApartmentsCachedService();
            _priceListService = new PriceListService();
        }

        public BookingCalendarEvent[] GetBookingsByApartmentId(Guid id, SearchType? searchType)
        {
            using (var db = new OttostuganContext())
            {
                var bookings = db.Bookings.Where(x => x.ApartmentId == id).ToArray().Select(x => x.ToBookingEntity()).ToArray();

                if (searchType.HasValue && searchType.Value == SearchType.FullWeek)
                {
                    bookings = GroupBookingsByWeek(bookings);
                }

                return bookings.Select(ToCalendarEvent).ToArray();
            }
        }

        public BookingCalendarEvent[] GetBookingsByApartmentId(Guid id, SearchType? searchType, DateTime startDate, DateTime endDate, DateTime fromDateTime, DateTime toDateTime)
        {

            BookingEntity[] notAvailableBookings;
            List<BookingEntity> availableBookings;
            if (searchType.HasValue && searchType.Value == SearchType.ChooseFreely)
            {
                availableBookings = GenerateAvailableBookings(new[] { id }, fromDateTime, toDateTime).ToList();
            }
            else
            {
                availableBookings =
                GenerateAvailableBookings(new[] { id })
                    .Where(b => b.FromDate.Date >= startDate.Date && b.ToDate.Date < endDate.Date.AddDays(1))
                    .ToList();
            }
            


            var searchDates = new List<DateTime>();
            var iDate = startDate;

            while (iDate <= endDate)
            {
                searchDates.Add(iDate.Date);
                iDate = iDate.AddDays(1);
            }

            using (var db = new OttostuganContext())
            {
                var allBookings = db.Bookings.Where(x => x.ApartmentId == id).OrderBy(x => x.FromDate).ToList();
                //(p => p.Genres.Any(x => listOfGenres.Contains(x));
                var narrowedDown = allBookings.Where(a => searchDates.Any(b => a.Dates.Contains(b))).ToList();
                notAvailableBookings = narrowedDown.Select(x => x.ToBookingEntity()).ToArray();
            }

            var mergedBookings = MergeBookings(availableBookings, notAvailableBookings);

            BookingEntity[] bookings = new BookingEntity[0];

            if (searchType.HasValue && searchType.Value == SearchType.ChooseFreely)
            {
                //List<BookingEntity> notAvailableBookingsList = notAvailableBookings.OfType<BookingEntity>().ToList();
                if (fromDateTime.Year > 2000 && toDateTime.Year > 2000) { 
                    mergedBookings.AddRange(notAvailableBookings.Where(a => a.ToDate >= DateTime.Now.Date).ToList());
                }
                else
                {
                    bookings = notAvailableBookings.Where(a => a.ToDate >= DateTime.Now.Date).ToArray();
                    var cEvents = bookings.Select(ToCalendarEvent).ToList();
                    foreach (var ceEvent in cEvents)
                    {
                        ceEvent.Title = "Ej ledig";
                    }
                    return cEvents.ToArray();
                }
                bookings = mergedBookings.ToArray();// notAvailableBookings.Where(b => b.FromDate >= DateTime.Today.Date).ToArray();
                var calenderEvents = bookings.Select(ToCalendarEvent).ToList();
                foreach (var calenderEvent in calenderEvents)
                {
                    if (calenderEvent.CanBeSelected || (int)calenderEvent.Price < 0)
                    {
                        calenderEvent.Title = "Ledig - Skicka prisförfrågan";// " + calenderEvent.Price.ToString() + " kr";
                        calenderEvent.FormattedPrice = "Skicka prisförfrågan";
                        calenderEvent.Price = -1;
                        calenderEvent.CanBeSelected = true;
                        calenderEvent.Color = GetEventColor(BookingStatus.Available);
                        calenderEvent.TextColor = GetEventTextColor(BookingStatus.Available);
                    } else {//if (calenderEvent.Start.Equals(fromDateTime.ToString("yyyy-MM-dd 12:00")) && calenderEvent.End.Equals(toDateTime.ToString("yyyy-MM-dd 12:00"))) {
                        calenderEvent.Title = "Ej ledig";
                    }
                }

                return calenderEvents.ToArray();
            }
            else if (searchType.HasValue && searchType.Value == SearchType.FullWeek)
            {
                bookings = GroupBookingsByWeek(mergedBookings.ToArray());
            }
            else
            {
                bookings = mergedBookings.ToArray();
            }

            return bookings.Select(ToCalendarEvent).ToArray();
        }

        public ApartmentBooking[] SearchApartments(int? adults, int? children, DateTime startDate, DateTime endDate, BookingStatus? status, SearchType? searchType)
        {

            BookingEntity[] notAvailableBookings;
            List<BookingEntity> availableBookings;
            if (searchType.HasValue && searchType.Value == SearchType.ChooseFreely)
            {
                availableBookings = GenerateAvailableBookings(null, startDate, endDate).ToList();
            }
            else
            {
                availableBookings = GenerateAvailableBookings().Where(b => b.FromDate.Date >= startDate.Date && b.ToDate.Date < endDate.Date.AddDays(1)).ToList();
            }

            using (var db = new OttostuganContext())
            {
                var query = db.Bookings.ToArray().Where(b => b.FromDate.Date < endDate.Date && startDate.Date < b.ToDate.Date).Select(x => x.ToBookingEntity());

                notAvailableBookings = query.ToArray();
            }

            var mergedBookings = MergeBookings(availableBookings, notAvailableBookings);

            BookingEntity[] bookings = new BookingEntity[0];

            if (searchType.HasValue && searchType.Value == SearchType.ChooseFreely)
            {
                bookings = mergedBookings.ToArray();
            }
            else if (searchType.HasValue && searchType.Value == SearchType.FullWeek)
            {
                bookings = GroupBookingsByWeek(mergedBookings.ToArray());
            }
            else
            {
                bookings = mergedBookings.ToArray();
            }

            if (searchType.HasValue && searchType.Value == SearchType.ChooseFreely)
            {
                bookings = mergedBookings.ToArray();
                foreach (var booking in bookings)
                {
                    booking.Status = BookingStatus.Available;
                }
            } else
            {
                LoadBookingPrice(bookings);
            }
            

            return bookings.Where(b => b.Status == BookingStatus.Available).Select(booking => new ApartmentBooking() { Apartment = GetApartmentById(booking.ApartmentId), Booking = booking }).Where(ab => ab.Apartment != null).Where(ab => IsSuitableApartment(ab.Apartment, adults)).ToArray();
        }

        private List<BookingEntity> MergeBookings(List<BookingEntity> availableBookings, IEnumerable<BookingEntity> notAvailableBookings)
        {
            foreach (var booking in notAvailableBookings)
            {

                BookingEntity booking1 = booking;

                //var bookings = availableBookings.Where(a =>
                //    a.ApartmentId == booking1.ApartmentId
                //    && a.FromDate.Date >= booking1.ToDate.Date
                //    && booking1.FromDate.Date <= a.ToDate.Date).ToArray();

                var bookings = availableBookings.Where(a =>
                    a.ApartmentId == booking1.ApartmentId
                    && a.FromDate.Date >= booking1.FromDate.Date
                    && a.ToDate.Date <= booking1.ToDate.Date).ToArray();

                //var bookings = availableBookings.Where(a =>
                //    a.ApartmentId == booking1.ApartmentId
                //    && a.FromDate.Date >= booking1.FromDate.Date
                //    && a.ToDate.Date <= booking1.ToDate.Date).ToArray();


                bookings.Each((i, entity) =>
                {
                    entity.Status = booking1.Status;
                });
            }

            return availableBookings;
        }

        public BookingEntity GetBookingById(int bookingId)
        {
            using (var db = new OttostuganContext())
            {
                var booking = db.Bookings.FirstOrDefault(b => b.Id == bookingId);

                if (booking == null)
                    return null;

                var entity = booking.ToBookingEntity();

                decimal price;

                if (_priceListService.TryGetBookingPrice(entity, out price))
                {
                    entity.Price = price;
                }

                return entity;
            }
        }

        private BookingEntity[] GroupBookingsByWeek(BookingEntity[] bookings)
        {
            var groupsByApartment = bookings.GroupBy(b => b.ApartmentId);

            var result = new List<BookingEntity>();

            foreach (var group in groupsByApartment)
            {
                var bs = group.OrderBy(b => b.FromDate);

                foreach (var booking in bs)
                {
                    if (booking.FromDate.DayOfWeek != DayOfWeek.Sunday)
                    {
                        continue;
                    }

                    var sibling = bs.FirstOrDefault(b => b.FromDate == booking.ToDate);

                    if (sibling != null)
                    {
                        var fullWeekBooking = booking;
                        fullWeekBooking.ToDate = sibling.ToDate;
                        fullWeekBooking.Status = booking.Status == BookingStatus.Available &&
                                                 sibling.Status == BookingStatus.Available
                            ? BookingStatus.Available
                            : booking.Status == BookingStatus.Available ? sibling.Status : booking.Status;


                        result.Add(fullWeekBooking);
                    }

                }

            }

            return result.ToArray();
        }

        private void LoadBookingPrice(IEnumerable<BookingEntity> bookings)
        {
            foreach (var booking in bookings)
            {
                decimal price = 0;

                var isPriceAvailable = _priceListService.TryGetBookingPrice(booking, out price);

                BookingStatus st = booking.Status;

                if (!isPriceAvailable)
                {
                    st = BookingStatus.NotAvalable;
                }

                booking.Status = st;

                booking.Price = price;
            }
        }

        private List<BookingEntity> GenerateAvailableBookings(IEnumerable<Guid> apartmentIds = null, DateTime fromDate = new DateTime(), DateTime toDate = new DateTime())
        {
            List<BookingEntity> availableBookings = new List<BookingEntity>();

            var query = _apartmentsCachedService.GetApartmetPages().AsQueryable();

            if (apartmentIds != null)
            {
                query = query.Where(p => apartmentIds.Contains(p.Id));
            }

            query = query.OrderBy(p => p.Seqno);

            var apartments = query.ToList();

            List<ApartmentPageModel> apartmentPageModels =
                apartments.Select(apartment => _apartmentsCachedService.GetApartmentPageModel(apartment.Permalink.Name))
                    .ToList();

            foreach (var am in apartmentPageModels)
            {
                if (am == null || am.PriceList == null || am.PriceList.PriceListRows == null || !am.PriceList.PriceListRows.Any())
                    continue;

                if (fromDate.Year > 2000 && toDate.Year > 2000) //ChooseFreely custom dates
                {
                    availableBookings.Add(CommonHelper.ToBookingEntity(new Booking
                    {
                        FromDate = fromDate,
                        ToDate = toDate,
                        ApartmentId = am.Page.Id,
                        BookedByEmail = string.Empty,
                        BookedByName = string.Empty,
                        BookedByPhone = string.Empty,
                        Comments = string.Empty,
                        CreatedDate = DateTime.Now,
                        Status = BookingStatus.Available,
                        IsPaidDeposit = false,
                        IsPaidFully = false
                    }));
                } else { 
                    foreach (var priceListRow in am.PriceList.PriceListRows)
                    {

                        var ranges = SplitDateRange(priceListRow.FromDate, priceListRow.ToDate).Where(x => x.Item1.Date >= DateTime.Now.Date).ToList();
                        availableBookings.AddRange(ranges.Select(range => new Booking
                        {
                            FromDate = range.Item1,
                            ToDate = range.Item2,
                            ApartmentId = am.Page.Id,
                            BookedByEmail = string.Empty,
                            BookedByName = string.Empty,
                            BookedByPhone = string.Empty,
                            Comments = string.Empty,
                            CreatedDate = DateTime.Now,
                            Status = BookingStatus.Available,
                            IsPaidDeposit = false,
                            IsPaidFully = false
                        }).Select(x => x.ToBookingEntity()));
                    
                    }
                }
            }

            return availableBookings;
        }

        public BookingCalendarEvent[] SearchBookings(int? adults, int? children, SearchType? searchType, DateTime startDate, DateTime endDate, DateTime fromDateTime, DateTime toDateTime)
        {
            BookingEntity[] notAvailableBookings;

            List<BookingEntity> availableBookings;
            if (searchType.HasValue && searchType.Value == SearchType.ChooseFreely)
            {
                availableBookings = GenerateAvailableBookings(null, fromDateTime, toDateTime).ToList();
            }
            else
            {
                availableBookings = GenerateAvailableBookings();
            }

            using (var db = new OttostuganContext())
            {
                notAvailableBookings = db.Bookings.ToArray().Where(b => b.FromDate.Date >= startDate.Date && b.ToDate.Date < endDate.Date.AddDays(10)).Select(x => x.ToBookingEntity()).ToArray();
            }

            var mergedBookings = MergeBookings(availableBookings, notAvailableBookings);

            BookingEntity[] bookings = new BookingEntity[0];

            if (searchType.HasValue && searchType.Value == SearchType.ChooseFreely)
            {
                //List<BookingEntity> notAvailableBookingsList = notAvailableBookings.OfType<BookingEntity>().ToList();
                //if (fromDateTime.Year > 2000 && toDateTime.Year > 2000)
                //    mergedBookings.AddRange(notAvailableBookings.Where(a => a.ToDate >= DateTime.Now.Date).ToList());
                if (fromDateTime.Year > 2000 && toDateTime.Year > 2000) { 
                    bookings = mergedBookings.ToArray();
                }
                else
                {
                    BookingEntity[] emptyArray = new BookingEntity[0];
                     bookings = emptyArray;

                }
            // notAvailableBookings.Where(b => b.FromDate >= DateTime.Today.Date).ToArray();
                //var calenderEvents = bookings.Select(ToCalendarEvent).ToList();
                //foreach (var calenderEvent in calenderEvents)
                //{
                //    if (calenderEvent.CanBeSelected)
                //    {
                //        calenderEvent.Title = "Ledig - " + calenderEvent.Price.ToString() + " kr";
                //    }
                //    else if (calenderEvent.Start.Equals(fromDateTime.ToString("yyyy-MM-dd 12:00")) && calenderEvent.End.Equals(toDateTime.ToString("yyyy-MM-dd 12:00")))
                //    {
                //        calenderEvent.Title = "Ej ledig";
                //    }
                //}

                //return calenderEvents.ToArray();
            }
            else if (searchType.HasValue && searchType.Value == SearchType.FullWeek)
            {
                bookings = GroupBookingsByWeek(mergedBookings.ToArray());
            }
            else
            {
                bookings = mergedBookings.ToArray();
            }

            LoadBookingPrice(bookings);

            var suitableBookings = new List<BookingEntity>();

            foreach (var booking in bookings)
            {
                var apartment = GetApartmentById(booking.ApartmentId);

                if (apartment != null && IsSuitableApartment(apartment, adults/*, children*/))
                {
                    if (!suitableBookings.Any(b => b.FromDate == booking.FromDate && b.ToDate == booking.ToDate))
                    {
                        suitableBookings.Add(booking);
                    }
                    else
                    {
                        var sb =
                            suitableBookings.First(b => b.FromDate == booking.FromDate && b.ToDate == booking.ToDate);

                        if (booking.Status == BookingStatus.Available)
                        {
                            if (sb.Status == BookingStatus.Available)
                            {
                                if (sb.Price > booking.Price)
                                {
                                    suitableBookings.Remove(sb);
                                    suitableBookings.Add(booking);
                                }
                            }
                            else
                            {
                                suitableBookings.Remove(sb);
                                suitableBookings.Add(booking);
                            }
                        }
                        else if (booking.Status == BookingStatus.Booked && sb.Status == BookingStatus.NotAvalable)
                        {
                            suitableBookings.Remove(sb);
                            suitableBookings.Add(booking);
                        }
                    }
                }
            }

            var calenderEvents = suitableBookings.Select(ToCalendarEvent).OrderBy(b => b.Start).ToList();
            foreach (var calenderEvent in calenderEvents)
            {
                if (calenderEvent.CanBeSelected || (int)calenderEvent.Price == 0)
                {
                    calenderEvent.Title = "Lediga lägenheter";// " + calenderEvent.Price.ToString() + " kr";
                    calenderEvent.CanBeSelected = true;
                    calenderEvent.Color = GetEventColor(BookingStatus.Available);
                    calenderEvent.TextColor = GetEventTextColor(BookingStatus.Available);
                }
                else if (calenderEvent.Start.Equals(fromDateTime.ToString("yyyy-MM-dd 12:00")) && calenderEvent.End.Equals(toDateTime.ToString("yyyy-MM-dd 12:00")))
                {
                    calenderEvent.Title = "Ej ledig";
                }
            }

            return calenderEvents.ToArray();
        }

        private bool IsSuitableApartment(Page apartment, int? adults)
        {
            var isSuitable = true;

            if (adults.HasValue)
            {
                if (apartment.Properties.Any(p => p.Name == "Adult beds") || apartment.Properties.Any(p => p.Name == "Extra beds"))
                {
                    var adultBedsPropertyValue = apartment.Properties.Any(p => p.Name == "Adult beds") ? apartment.Properties.First(p => p.Name == "Adult beds").Value : null;
                    var childrenBedsPropertyValue = apartment.Properties.Any(p => p.Name == "Extra beds")
                        ? apartment.Properties.First(p => p.Name == "Extra beds").Value
                        : null;

                    int adultBedsCount = 0;
                    int childrenBedsCount = 0;

                    var parsedAdult = int.TryParse(adultBedsPropertyValue, out adultBedsCount);
                    var parsedChildren = int.TryParse(childrenBedsPropertyValue, out childrenBedsCount);

                    if (adultBedsPropertyValue == null && childrenBedsPropertyValue == null)
                    {
                        isSuitable = false;
                    }
                    else if (parsedAdult || parsedChildren)
                    {
                        var res = adultBedsCount + childrenBedsCount;
                        if (adults.Value > res)
                        {
                            isSuitable = false;
                        }
                    }
                }
                else
                {
                    isSuitable = false;
                }
            }

            return isSuitable;
        }

        private Page GetApartmentById(Guid apartmentId)
        {
            return _apartmentsCachedService.GetApartmentById(apartmentId);
        }

        private BookingCalendarEvent ToCalendarEvent(BookingEntity booking)
        {
            decimal price;

            BookingStatus status = booking.Status;

            var isPriceAvailable = _priceListService.TryGetBookingPrice(booking, out price);

            if (!isPriceAvailable)
            {
                status = BookingStatus.NotAvalable;
            }

            return new BookingCalendarEvent
            {
                BookingId = booking.Id,
                Title = GetEventTitle(booking, price, status),
                AllDay = false,
                Start = booking.FromDate.ToString("yyyy-MM-dd 12:00"),
                End = booking.ToDate.ToString("yyyy-MM-dd 12:00"),
                Color = GetEventColor(status),
                TextColor = GetEventTextColor(status),
                CanBeSelected = status == BookingStatus.Available,
                Price = booking.Price < 1 ? -1 : price,
                FormattedPrice = CommonHelper.FormatPrice(price)
            };
        }

        private string GetEventTextColor(BookingStatus status)
        {
            string color = string.Empty;
            switch (status)
            {
                case BookingStatus.Available:
                    color = "#ffffff";
                    break;
                case BookingStatus.Booked:
                case BookingStatus.NotAvalable:
                    color = "#888888";
                    break;

            }
            return color;
        }

        private string GetEventColor(BookingStatus status)
        {
            string color = string.Empty;
            switch (status)
            {
                case BookingStatus.Available:
                    color = "#70d989";
                    break;
                case BookingStatus.Booked:
                case BookingStatus.NotAvalable:
                    color = "#d7d7d7";
                    break;

            }
            return color;
        }

        private string GetEventTitle(BookingEntity booking, decimal price, BookingStatus bookingStatus)
        {
            string result = Resources.Labels.BookingEventTitleFormat;

            string startDay = booking.FromDate.DayOfWeek == DayOfWeek.Sunday ? Resources.Labels.Sunday : Resources.Labels.Thursday;
            string endDay = booking.ToDate.DayOfWeek == DayOfWeek.Sunday ? Resources.Labels.Sunday : Resources.Labels.Thursday;

            string status = string.Empty;


            switch (bookingStatus)
            {
                case BookingStatus.Available:
                    status = string.Format("{0} {1}", Resources.Labels.BookingIsAvailable, CommonHelper.FormatPrice(price));
                    break;
                case BookingStatus.NotAvalable:
                    status = Resources.Labels.BookingIsUnavailable;
                    break;
                case BookingStatus.Booked:
                    status = Resources.Labels.BookingIsUnavailable;
                    break;
            }

            return string.Format(result, startDay, endDay, status);
        }

        public static IEnumerable<Tuple<DateTime, DateTime>> SplitDateRange(DateTime start, DateTime end)
        {
            DateTime chunkEnd = start.AddDays(1);
            while (chunkEnd < end)
            {
                if (chunkEnd.DayOfWeek == DayOfWeek.Thursday || chunkEnd.DayOfWeek == DayOfWeek.Sunday)
                {
                    yield return Tuple.Create(start, chunkEnd);
                    start = chunkEnd;
                }
                chunkEnd = chunkEnd.AddDays(1);
            }
            yield return Tuple.Create(start, end);
        }
    }
}