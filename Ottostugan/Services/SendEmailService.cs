﻿using System.Configuration;
using System.Net;
using System.Net.Mail;

namespace Ottostugan.Services
{
    public class SendEmailService : ISendEmailService
    {
        public void SendEmail(string subject, string body)
        {
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            var mail = new MailMessage();
            var smtpServer = new SmtpClient(ConfigurationManager.AppSettings["SmtpServerAddress"])
            {
                Credentials = new NetworkCredential(ConfigurationManager.AppSettings["SmtpServerUserName"], ConfigurationManager.AppSettings["SmtpServerPassword"]),
                Port = 587,
                EnableSsl = true
            };

            mail.From = new MailAddress(ConfigurationManager.AppSettings["SmtpServerUserName"]);
            mail.To.Add(ConfigurationManager.AppSettings["SendBookingToEmail"]);

            mail.Subject = subject;

            mail.Body = body;

            smtpServer.Send(mail);
        }
    }
}