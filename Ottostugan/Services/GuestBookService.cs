using System;
using System.Linq;
using Ottostugan.DAL;
using Ottostugan.Models;

namespace Ottostugan.Services
{
    public class GuestBookService : IGuestBookService
    {
        public GuestFeedback[] GetPublishedFeedbacks()
        {
            using (var db = new OttostuganContext())
            {
                return db.GuestFeedbacks.Where(f => f.Published).OrderByDescending(f=>f.CreatedDate).ToArray();
            }
        }

        public bool SaveFeedback(GuestFeedbackViewModel model)
        {
            GuestFeedback feedback = new GuestFeedback
            {
                Comment = model.Comment,
                From = model.From,
                Published = false,
                CreatedDate = DateTime.Now
            };

            using (var db = new OttostuganContext())
            {
                db.GuestFeedbacks.Add(feedback);
                db.SaveChanges();
            }

            return true;
        }
    }
}