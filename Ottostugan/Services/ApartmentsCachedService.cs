﻿using System;
using System.Data.Entity;
using System.Linq;
using Ottostugan.Caching;
using Ottostugan.Extensions.PageModels;
using Ottostugan.Extensions.PageTypes;
using Piranha.Entities;

namespace Ottostugan.Services
{
    public class ApartmentsCachedService : IApartmentsCachedService
    {
        private readonly ICacheService _cacheService;

        private const string ApartmentPagesCacheId = "AllApartmentPages";
        private const string ApartmentPageModelsCacheId = "ApartmentPageModels";

        public ApartmentsCachedService()
        {
            _cacheService = new InMemoryCache();
        }

        public Page[] GetApartmetPages()
        {
            return _cacheService.Get(ApartmentPagesCacheId, FetchApartmentPagesFromDb);
        }

        public Page GetApartmentById(Guid id)
        {
            return _cacheService.Get(ApartmentPagesCacheId, FetchApartmentPagesFromDb).FirstOrDefault(apartment => apartment.Id == id);
        }

        public ApartmentPageModel GetApartmentPageModel(string permalink)
        {
            return
                _cacheService.Get(ApartmentPageModelsCacheId, FetchApartmentPageModelsFromDb)
                    .FirstOrDefault(x => x.Page.Permalink == permalink);
        }

        private ApartmentPageModel[] FetchApartmentPageModelsFromDb()
        {
            var pages = GetApartmetPages();

            var models = 
                pages.Select(x => Piranha.Models.PageModel.GetByPermalink<ApartmentPageModel>(x.Permalink.Name))
                    .ToArray();

            models.Each((i, model) => model.LoadPriceList());

            return models;
        }

        private Page[] FetchApartmentPagesFromDb()
        {
            using (var db = new Piranha.DataContext())
            {
                var query = db.Pages
                    .Include("Permalink")
                    .Include("Template")
                    .Include("Properties")
                    .Include("Regions")
                    .Where(p => p.Template.Type == (typeof (ApartmentPage)).FullName);

                return query.ToArray();
            }
        }
    }
}